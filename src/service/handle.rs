use axum::response;

async fn get_list() -> response::Html<&'static str> {
    log::info!("{:?}", response::Html("<h1>Hello, World!</h1>"));
    response::Html("<h1>Hello, World!</h1>")
}
