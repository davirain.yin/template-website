use crate::db_manager::{Db, RefVeveType, VeveType};
use crate::series::cartoon_network::adventure_time::s1::{
    AdventureTime, FinnMertens, IceKing, JakeTheDog, Marceline,
};
use crate::series::common_type::Series;
use crate::series::Object;
use uuid::Uuid;

pub fn create(value: Series, db: Db) {
    match value {
        Series::AdventureTime(val) => match val {
            crate::series::common_type::AdventureTime::S1 => {
                let obj1 = AdventureTime::Marceline(Marceline::new(Uuid::new_v4()));
                println!("Obj1: {:?}", obj1);
                let obj2 = AdventureTime::FinnMertens(FinnMertens::new(Uuid::new_v4()));
                println!("Obj2: {:?}", obj2);
                let obj3 = AdventureTime::IceKing(IceKing::new(Uuid::new_v4()));
                println!("Obj3: {:?}", obj3);
                let obj4 = AdventureTime::JakeTheDog(JakeTheDog::new(Uuid::new_v4()));
                println!("Obj4: {:?}", obj4);
                db.write().unwrap().insert(obj1.get_uuid(), Box::new(obj1));
                db.write().unwrap().insert(obj2.get_uuid(), Box::new(obj2));
                db.write().unwrap().insert(obj3.get_uuid(), Box::new(obj3));
                db.write().unwrap().insert(obj4.get_uuid(), Box::new(obj4));
            }
            crate::series::common_type::AdventureTime::S2 => {
                unimplemented!()
            }
        },
        _ => unimplemented!(),
    }
}

#[test]
fn test_create() {
    let db = Db::default();
    let series = Series::AdventureTime(crate::series::common_type::AdventureTime::S1);

    create(series, db.clone());

    println!("db: {:#?}", db);
}
