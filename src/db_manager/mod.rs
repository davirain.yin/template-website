use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
use crate::series::Object;
use std::collections::HashMap;
use std::fmt::Debug;
use std::sync::{Arc, RwLock};
use uuid::Uuid;

pub type VeveType = Box<
    dyn Object<
        Name = String,
        DropDate = String,
        ListPrice = f32,
        Editions = u32,
        EditionType = EditionType,
        Season = Season,
        Rarity = Rarity,
        License = License,
        Brand = Brand,
        Series = Series,
        Uuid = Uuid,
    >,
>;

pub type RefVeveType = Box<
    &'static dyn Object<
        Name = String,
        DropDate = String,
        ListPrice = f32,
        Editions = u32,
        EditionType = EditionType,
        Season = Season,
        Rarity = Rarity,
        License = License,
        Brand = Brand,
        Series = Series,
        Uuid = Uuid,
    >,
>;

pub type Db = Arc<RwLock<HashMap<Uuid, VeveType>>>;

pub mod create;
pub mod query;
