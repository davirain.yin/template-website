#![feature(allocator_api)]
#![allow(unused_variables)]
#![allow(private_in_public)]
#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(clippy::upper_case_acronyms)]

pub mod db_manager;
pub mod query_token_info;
pub mod series;
pub mod service;

use axum::prelude::*;
use std::net::SocketAddr;

#[tokio::main]
async fn main() {
    env_logger::init();

    // build our application with a route
    let app = route("/", get(handler));

    // run it
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    log::info!("addr: {}", addr);

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn handler() -> response::Html<&'static str> {
    log::info!("{:?}", response::Html("<h1>Hello, World!</h1>"));
    response::Html("<h1>Hello, World!</h1>")
}
