mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum Cryptkins {
        TwilightMothman(TwilightMothman),
        Bigfoot(Bigfoot),
        Yeti(Yeti),
        Nightcrawler(Nightcrawler),
        Thunderbird(Thunderbird),
        Nessie(Nessie),
        Chupacabra(Chupacabra),
        JerseyDevil(JerseyDevil),
        Mothman(Mothman),
        Ogopogo(Ogopogo),
        Cthulhu(Cthulhu),
        CosmicCthulhu(CosmicCthulhu),
    }

    impl Object for Cryptkins {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                Cryptkins::TwilightMothman(val) => val.name.clone(),
                Cryptkins::Bigfoot(val) => val.name.clone(),
                Cryptkins::Yeti(val) => val.name.clone(),
                Cryptkins::Nightcrawler(val) => val.name.clone(),
                Cryptkins::Thunderbird(val) => val.name.clone(),
                Cryptkins::Nessie(val) => val.name.clone(),
                Cryptkins::Chupacabra(val) => val.name.clone(),
                Cryptkins::JerseyDevil(val) => val.name.clone(),
                Cryptkins::Mothman(val) => val.name.clone(),
                Cryptkins::Ogopogo(val) => val.name.clone(),
                Cryptkins::Cthulhu(val) => val.name.clone(),
                Cryptkins::CosmicCthulhu(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                Cryptkins::TwilightMothman(val) => val.drop_date.clone(),
                Cryptkins::Bigfoot(val) => val.drop_date.clone(),
                Cryptkins::Yeti(val) => val.drop_date.clone(),
                Cryptkins::Nightcrawler(val) => val.drop_date.clone(),
                Cryptkins::Thunderbird(val) => val.drop_date.clone(),
                Cryptkins::Nessie(val) => val.drop_date.clone(),
                Cryptkins::Chupacabra(val) => val.drop_date.clone(),
                Cryptkins::JerseyDevil(val) => val.drop_date.clone(),
                Cryptkins::Mothman(val) => val.drop_date.clone(),
                Cryptkins::Ogopogo(val) => val.drop_date.clone(),
                Cryptkins::Cthulhu(val) => val.drop_date.clone(),
                Cryptkins::CosmicCthulhu(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                Cryptkins::TwilightMothman(val) => val.list_price.clone(),
                Cryptkins::Bigfoot(val) => val.list_price.clone(),
                Cryptkins::Yeti(val) => val.list_price.clone(),
                Cryptkins::Nightcrawler(val) => val.list_price.clone(),
                Cryptkins::Thunderbird(val) => val.list_price.clone(),
                Cryptkins::Nessie(val) => val.list_price.clone(),
                Cryptkins::Chupacabra(val) => val.list_price.clone(),
                Cryptkins::JerseyDevil(val) => val.list_price.clone(),
                Cryptkins::Mothman(val) => val.list_price.clone(),
                Cryptkins::Ogopogo(val) => val.list_price.clone(),
                Cryptkins::Cthulhu(val) => val.list_price.clone(),
                Cryptkins::CosmicCthulhu(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                Cryptkins::TwilightMothman(val) => val.editions.clone(),
                Cryptkins::Bigfoot(val) => val.editions.clone(),
                Cryptkins::Yeti(val) => val.editions.clone(),
                Cryptkins::Nightcrawler(val) => val.editions.clone(),
                Cryptkins::Thunderbird(val) => val.editions.clone(),
                Cryptkins::Nessie(val) => val.editions.clone(),
                Cryptkins::Chupacabra(val) => val.editions.clone(),
                Cryptkins::JerseyDevil(val) => val.editions.clone(),
                Cryptkins::Mothman(val) => val.editions.clone(),
                Cryptkins::Ogopogo(val) => val.editions.clone(),
                Cryptkins::Cthulhu(val) => val.editions.clone(),
                Cryptkins::CosmicCthulhu(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            Self::EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Self::Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                Cryptkins::TwilightMothman(val) => Rarity::Uncommon,
                Cryptkins::Bigfoot(val) => Rarity::Common,
                Cryptkins::Yeti(val) => Rarity::Common,
                Cryptkins::Nightcrawler(val) => Rarity::Rare,
                Cryptkins::Thunderbird(val) => Rarity::Rare,
                Cryptkins::Nessie(val) => Rarity::Common,
                Cryptkins::Chupacabra(val) => Rarity::Common,
                Cryptkins::JerseyDevil(val) => Rarity::Uncommon,
                Cryptkins::Mothman(val) => Rarity::Uncommon,
                Cryptkins::Ogopogo(val) => Rarity::Rare,
                Cryptkins::Cthulhu(val) => Rarity::Uncommon,
                Cryptkins::CosmicCthulhu(val) => Rarity::UltraRare,
            }
        }

        fn get_license(&self) -> Self::License {
            License::Cryptozoic
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::Cryptkins
        }

        fn get_series(&self) -> Self::Series {
            Series::Cryptkins(crate::series::common_type::Cryptkins::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                Cryptkins::TwilightMothman(val) => val.id.clone(),
                Cryptkins::Bigfoot(val) => val.id.clone(),
                Cryptkins::Yeti(val) => val.id.clone(),
                Cryptkins::Nightcrawler(val) => val.id.clone(),
                Cryptkins::Thunderbird(val) => val.id.clone(),
                Cryptkins::Nessie(val) => val.id.clone(),
                Cryptkins::Chupacabra(val) => val.id.clone(),
                Cryptkins::JerseyDevil(val) => val.id.clone(),
                Cryptkins::Mothman(val) => val.id.clone(),
                Cryptkins::Ogopogo(val) => val.id.clone(),
                Cryptkins::Cthulhu(val) => val.id.clone(),
                Cryptkins::CosmicCthulhu(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct TwilightMothman {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl TwilightMothman {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Bigfoot {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Bigfoot {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Yeti {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Yeti {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Nightcrawler {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Nightcrawler {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Thunderbird {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Thunderbird {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Nessie {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Nessie {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Chupacabra {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Chupacabra {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct JerseyDevil {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl JerseyDevil {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Mothman {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Mothman {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Ogopogo {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Ogopogo {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Cthulhu {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Cthulhu {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct CosmicCthulhu {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl CosmicCthulhu {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
