mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum Labbit {
        SmorkinLabbitChoiceCuts(SmorkinLabbitChoiceCuts),
        BackstabSmorkinLabbit(BackstabSmorkinLabbit),
        TheVisibleLabbitNeonEdition(TheVisibleLabbitNeonEdition),
        SmorkinLabbitRed(SmorkinLabbitRed),
        TheVisbleLabbit(TheVisbleLabbit),
        HappyStacheLabbit(HappyStacheLabbit),
    }

    impl Object for Labbit {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                Labbit::SmorkinLabbitChoiceCuts(val) => val.name.clone(),
                Labbit::BackstabSmorkinLabbit(val) => val.name.clone(),
                Labbit::TheVisibleLabbitNeonEdition(val) => val.name.clone(),
                Labbit::SmorkinLabbitRed(val) => val.name.clone(),
                Labbit::TheVisbleLabbit(val) => val.name.clone(),
                Labbit::HappyStacheLabbit(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                Labbit::SmorkinLabbitChoiceCuts(val) => val.drop_date.clone(),
                Labbit::BackstabSmorkinLabbit(val) => val.drop_date.clone(),
                Labbit::TheVisibleLabbitNeonEdition(val) => val.drop_date.clone(),
                Labbit::SmorkinLabbitRed(val) => val.drop_date.clone(),
                Labbit::TheVisbleLabbit(val) => val.drop_date.clone(),
                Labbit::HappyStacheLabbit(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                Labbit::SmorkinLabbitChoiceCuts(val) => val.list_price.clone(),
                Labbit::BackstabSmorkinLabbit(val) => val.list_price.clone(),
                Labbit::TheVisibleLabbitNeonEdition(val) => val.list_price.clone(),
                Labbit::SmorkinLabbitRed(val) => val.list_price.clone(),
                Labbit::TheVisbleLabbit(val) => val.list_price.clone(),
                Labbit::HappyStacheLabbit(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                Labbit::SmorkinLabbitChoiceCuts(val) => val.editions.clone(),
                Labbit::BackstabSmorkinLabbit(val) => val.editions.clone(),
                Labbit::TheVisibleLabbitNeonEdition(val) => val.editions.clone(),
                Labbit::SmorkinLabbitRed(val) => val.editions.clone(),
                Labbit::TheVisbleLabbit(val) => val.editions.clone(),
                Labbit::HappyStacheLabbit(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstEdition
        }

        fn get_season(&self) -> Self::Season {
            Season::Season2
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                Labbit::SmorkinLabbitChoiceCuts(val) => Rarity::Common,
                Labbit::BackstabSmorkinLabbit(val) => Rarity::UltraRare,
                Labbit::TheVisibleLabbitNeonEdition(val) => Rarity::Rare,
                Labbit::SmorkinLabbitRed(val) => Rarity::Uncommon,
                Labbit::TheVisbleLabbit(val) => Rarity::Rare,
                Labbit::HappyStacheLabbit(val) => Rarity::Common,
            }
        }

        fn get_license(&self) -> Self::License {
            License::FrankKozik
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::Labbit
        }

        fn get_series(&self) -> Self::Series {
            Series::Labbit(crate::series::common_type::Labbit::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                Labbit::SmorkinLabbitChoiceCuts(val) => val.id.clone(),
                Labbit::BackstabSmorkinLabbit(val) => val.id.clone(),
                Labbit::TheVisibleLabbitNeonEdition(val) => val.id.clone(),
                Labbit::SmorkinLabbitRed(val) => val.id.clone(),
                Labbit::TheVisbleLabbit(val) => val.id.clone(),
                Labbit::HappyStacheLabbit(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct SmorkinLabbitChoiceCuts {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl SmorkinLabbitChoiceCuts {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct BackstabSmorkinLabbit {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl BackstabSmorkinLabbit {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct TheVisibleLabbitNeonEdition {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl TheVisibleLabbitNeonEdition {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct SmorkinLabbitRed {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl SmorkinLabbitRed {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct TheVisbleLabbit {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl TheVisbleLabbit {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct HappyStacheLabbit {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl HappyStacheLabbit {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
