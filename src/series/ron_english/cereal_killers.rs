mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum CerealKillers {
        HoneyButtTheObese(HoneyButtTheObese),
        CountCalorie(CountCalorie),
        FatTony(FatTony),
        CapnCornstarch(CapnCornstarch),
    }
    impl Object for CerealKillers {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                CerealKillers::HoneyButtTheObese(val) => val.name.clone(),
                CerealKillers::CountCalorie(val) => val.name.clone(),
                CerealKillers::FatTony(val) => val.name.clone(),
                CerealKillers::CapnCornstarch(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                CerealKillers::HoneyButtTheObese(val) => val.drop_date.clone(),
                CerealKillers::CountCalorie(val) => val.drop_date.clone(),
                CerealKillers::FatTony(val) => val.drop_date.clone(),
                CerealKillers::CapnCornstarch(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                CerealKillers::HoneyButtTheObese(val) => val.list_price.clone(),
                CerealKillers::CountCalorie(val) => val.list_price.clone(),
                CerealKillers::FatTony(val) => val.list_price.clone(),
                CerealKillers::CapnCornstarch(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                CerealKillers::HoneyButtTheObese(val) => val.editions.clone(),
                CerealKillers::CountCalorie(val) => val.editions.clone(),
                CerealKillers::FatTony(val) => val.editions.clone(),
                CerealKillers::CapnCornstarch(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Season::Season2
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                CerealKillers::HoneyButtTheObese(val) => Rarity::Rare,
                CerealKillers::CountCalorie(val) => Rarity::UltraRare,
                CerealKillers::FatTony(val) => Rarity::Common,
                CerealKillers::CapnCornstarch(val) => Rarity::Uncommon,
            }
        }

        fn get_license(&self) -> Self::License {
            License::RonEnglish
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::CerealKillers
        }

        fn get_series(&self) -> Self::Series {
            Series::CerealKillers(crate::series::common_type::CerealKillers::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                CerealKillers::HoneyButtTheObese(val) => val.id.clone(),
                CerealKillers::CountCalorie(val) => val.id.clone(),
                CerealKillers::FatTony(val) => val.id.clone(),
                CerealKillers::CapnCornstarch(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct HoneyButtTheObese {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl HoneyButtTheObese {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct CountCalorie {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl CountCalorie {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct FatTony {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl FatTony {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct CapnCornstarch {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl CapnCornstarch {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
