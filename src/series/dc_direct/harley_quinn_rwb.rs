mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum HarleyQuinnRWB {
        TerryDoson06(TerryDoson06),
        StevePugh19(StevePugh19),
        GuillemMarch18(GuillemMarch18),
        BabsTar08(BabsTar08),
    }

    impl Object for HarleyQuinnRWB {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                HarleyQuinnRWB::TerryDoson06(val) => val.name.clone(),
                HarleyQuinnRWB::StevePugh19(val) => val.name.clone(),
                HarleyQuinnRWB::GuillemMarch18(val) => val.name.clone(),
                HarleyQuinnRWB::BabsTar08(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                HarleyQuinnRWB::TerryDoson06(val) => val.drop_date.clone(),
                HarleyQuinnRWB::StevePugh19(val) => val.drop_date.clone(),
                HarleyQuinnRWB::GuillemMarch18(val) => val.drop_date.clone(),
                HarleyQuinnRWB::BabsTar08(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                HarleyQuinnRWB::TerryDoson06(val) => val.list_price.clone(),
                HarleyQuinnRWB::StevePugh19(val) => val.list_price.clone(),
                HarleyQuinnRWB::GuillemMarch18(val) => val.list_price.clone(),
                HarleyQuinnRWB::BabsTar08(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                HarleyQuinnRWB::TerryDoson06(val) => val.editions.clone(),
                HarleyQuinnRWB::StevePugh19(val) => val.editions.clone(),
                HarleyQuinnRWB::GuillemMarch18(val) => val.editions.clone(),
                HarleyQuinnRWB::BabsTar08(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstEdition
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                HarleyQuinnRWB::TerryDoson06(val) => Rarity::Rare,
                HarleyQuinnRWB::StevePugh19(val) => Rarity::UltraRare,
                HarleyQuinnRWB::GuillemMarch18(val) => Rarity::Uncommon,
                HarleyQuinnRWB::BabsTar08(val) => Rarity::Uncommon,
            }
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::HarleyQuinnRedWhiteAndBlack
        }

        fn get_series(&self) -> Self::Series {
            Series::HarleyQuinneRWB(crate::series::common_type::HarleyQuinneRWB::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                HarleyQuinnRWB::TerryDoson06(val) => val.id.clone(),
                HarleyQuinnRWB::StevePugh19(val) => val.id.clone(),
                HarleyQuinnRWB::GuillemMarch18(val) => val.id.clone(),
                HarleyQuinnRWB::BabsTar08(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct TerryDoson06 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl TerryDoson06 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct StevePugh19 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl StevePugh19 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct GuillemMarch18 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl GuillemMarch18 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct BabsTar08 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl BabsTar08 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
