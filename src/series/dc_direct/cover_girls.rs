mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum CoverGirls {
        Mera(Mera),
        Supergirl(Supergirl),
        Catwoman(Catwoman),
        Batgirl(Batgirl),
    }

    impl Object for CoverGirls {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                CoverGirls::Mera(val) => val.name.clone(),
                CoverGirls::Supergirl(val) => val.name.clone(),
                CoverGirls::Catwoman(val) => val.name.clone(),
                CoverGirls::Batgirl(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                CoverGirls::Mera(val) => val.drop_date.clone(),
                CoverGirls::Supergirl(val) => val.drop_date.clone(),
                CoverGirls::Catwoman(val) => val.drop_date.clone(),
                CoverGirls::Batgirl(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                CoverGirls::Mera(val) => val.list_price.clone(),
                CoverGirls::Supergirl(val) => val.list_price.clone(),
                CoverGirls::Catwoman(val) => val.list_price.clone(),
                CoverGirls::Batgirl(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                CoverGirls::Mera(val) => val.editions.clone(),
                CoverGirls::Supergirl(val) => val.editions.clone(),
                CoverGirls::Catwoman(val) => val.editions.clone(),
                CoverGirls::Batgirl(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Season::Season2
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                CoverGirls::Mera(val) => Rarity::Rare,
                CoverGirls::Supergirl(val) => Rarity::UltraRare,
                CoverGirls::Catwoman(val) => Rarity::Uncommon,
                CoverGirls::Batgirl(val) => Rarity::Common,
            }
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::DcCoverGirls
        }

        fn get_series(&self) -> Self::Series {
            Series::DcCoverGirls(crate::series::common_type::DcCoverGirls::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                CoverGirls::Mera(val) => val.id.clone(),
                CoverGirls::Supergirl(val) => val.id.clone(),
                CoverGirls::Catwoman(val) => val.id.clone(),
                CoverGirls::Batgirl(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct Mera {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Mera {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Supergirl {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Supergirl {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Catwoman {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Catwoman {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Batgirl {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Batgirl {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
