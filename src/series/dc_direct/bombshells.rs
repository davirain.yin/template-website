mod s2 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum S2 {
        Death(Death),
        HarleyQuinnDeluxe2(HarleyQuinnDeluxe2),
    }

    impl Object for S2 {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                S2::Death(val) => val.name.clone(),
                S2::HarleyQuinnDeluxe2(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                S2::Death(val) => val.drop_date.clone(),
                S2::HarleyQuinnDeluxe2(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                S2::Death(val) => val.list_price.clone(),
                S2::HarleyQuinnDeluxe2(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                S2::Death(val) => val.editions.clone(),
                S2::HarleyQuinnDeluxe2(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            match &self {
                S2::Death(val) => EditionType::FirstAppearance,
                S2::HarleyQuinnDeluxe2(val) => EditionType::None,
            }
        }

        fn get_season(&self) -> Self::Season {
            Season::Season2
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                S2::Death(val) => Rarity::UltraRare,
                S2::HarleyQuinnDeluxe2(val) => Rarity::Rare,
            }
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::Bombshells
        }

        fn get_series(&self) -> Self::Series {
            Series::Bombshell(crate::series::common_type::Bombshell::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                S2::Death(val) => val.id.clone(),
                S2::HarleyQuinnDeluxe2(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct Death {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Death {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
    #[derive(Debug)]
    pub struct HarleyQuinnDeluxe2 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl HarleyQuinnDeluxe2 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}

mod big_barda {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub struct BigBarda {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl BigBarda {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
    impl Object for BigBarda {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            self.name.clone()
        }

        fn get_drop_date(&self) -> Self::DropDate {
            self.drop_date.clone()
        }

        fn get_list_price(&self) -> Self::ListPrice {
            self.list_price.clone()
        }

        fn get_editions(&self) -> Self::Editions {
            self.editions.clone()
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            Rarity::Rare
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::Bombshells
        }

        fn get_series(&self) -> Self::Series {
            Series::BigBarda
        }

        fn get_uuid(&self) -> Self::Uuid {
            self.id.clone()
        }
    }
}

mod starfire {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub struct Starfire {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Starfire {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    impl Object for Starfire {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            self.name.clone()
        }

        fn get_drop_date(&self) -> Self::DropDate {
            self.drop_date.clone()
        }

        fn get_list_price(&self) -> Self::ListPrice {
            self.list_price.clone()
        }

        fn get_editions(&self) -> Self::Editions {
            self.editions.clone()
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            Rarity::Rare
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::Bombshells
        }

        fn get_series(&self) -> Self::Series {
            Series::Starfire
        }

        fn get_uuid(&self) -> Self::Uuid {
            self.id.clone()
        }
    }
}
