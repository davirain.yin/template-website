mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum BatmanBlackWhite {
        JoeMadureiraBatman97(JoeMadureiraBatman97),
        EduardoRissoBatman01(EduardoRissoBatman01),
        BackyCloonanBataman86(BackyCloonanBataman86),
        ToddMcFarlaneBatman100(ToddMcFarlaneBatman100),
    }

    impl Object for BatmanBlackWhite {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                BatmanBlackWhite::JoeMadureiraBatman97(val) => val.name.clone(),
                BatmanBlackWhite::EduardoRissoBatman01(val) => val.name.clone(),
                BatmanBlackWhite::BackyCloonanBataman86(val) => val.name.clone(),
                BatmanBlackWhite::ToddMcFarlaneBatman100(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                BatmanBlackWhite::JoeMadureiraBatman97(val) => val.drop_date.clone(),
                BatmanBlackWhite::EduardoRissoBatman01(val) => val.drop_date.clone(),
                BatmanBlackWhite::BackyCloonanBataman86(val) => val.drop_date.clone(),
                BatmanBlackWhite::ToddMcFarlaneBatman100(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                BatmanBlackWhite::JoeMadureiraBatman97(val) => val.list_price.clone(),
                BatmanBlackWhite::EduardoRissoBatman01(val) => val.list_price.clone(),
                BatmanBlackWhite::BackyCloonanBataman86(val) => val.list_price.clone(),
                BatmanBlackWhite::ToddMcFarlaneBatman100(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                BatmanBlackWhite::JoeMadureiraBatman97(val) => val.editions.clone(),
                BatmanBlackWhite::EduardoRissoBatman01(val) => val.editions.clone(),
                BatmanBlackWhite::BackyCloonanBataman86(val) => val.editions.clone(),
                BatmanBlackWhite::ToddMcFarlaneBatman100(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            match &self {
                BatmanBlackWhite::JoeMadureiraBatman97(val) => EditionType::FirstEdition,
                BatmanBlackWhite::EduardoRissoBatman01(val) => EditionType::FirstEdition,
                BatmanBlackWhite::BackyCloonanBataman86(val) => EditionType::FirstEdition,
                BatmanBlackWhite::ToddMcFarlaneBatman100(val) => EditionType::FirstAppearance,
            }
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                BatmanBlackWhite::JoeMadureiraBatman97(val) => Rarity::Uncommon,
                BatmanBlackWhite::EduardoRissoBatman01(val) => Rarity::UltraRare,
                BatmanBlackWhite::BackyCloonanBataman86(val) => Rarity::Rare,
                BatmanBlackWhite::ToddMcFarlaneBatman100(val) => Rarity::Common,
            }
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::BatmanBlackAndWhite
        }

        fn get_series(&self) -> Self::Series {
            Series::BatmanBlackAndWhite(crate::series::common_type::BatmanBlackAndWhite::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                BatmanBlackWhite::JoeMadureiraBatman97(val) => val.id.clone(),
                BatmanBlackWhite::EduardoRissoBatman01(val) => val.id.clone(),
                BatmanBlackWhite::BackyCloonanBataman86(val) => val.id.clone(),
                BatmanBlackWhite::ToddMcFarlaneBatman100(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct JoeMadureiraBatman97 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl JoeMadureiraBatman97 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct EduardoRissoBatman01 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl EduardoRissoBatman01 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct BackyCloonanBataman86 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl BackyCloonanBataman86 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct ToddMcFarlaneBatman100 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl ToddMcFarlaneBatman100 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}

mod s2 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum BatmanBlackWhite {
        JimLeeTheKoker21(JimLeeTheKoker21),
        CarmineInfantinoRobin72(CarmineInfantinoRobin72),
        DickSprangTheJoker61(DickSprangTheJoker61),
        JasonFabokBatman77(JasonFabokBatman77),
    }

    impl Object for BatmanBlackWhite {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                BatmanBlackWhite::JimLeeTheKoker21(val) => val.name.clone(),
                BatmanBlackWhite::CarmineInfantinoRobin72(val) => val.name.clone(),
                BatmanBlackWhite::DickSprangTheJoker61(val) => val.name.clone(),
                BatmanBlackWhite::JasonFabokBatman77(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                BatmanBlackWhite::JimLeeTheKoker21(val) => val.drop_date.clone(),
                BatmanBlackWhite::CarmineInfantinoRobin72(val) => val.drop_date.clone(),
                BatmanBlackWhite::DickSprangTheJoker61(val) => val.drop_date.clone(),
                BatmanBlackWhite::JasonFabokBatman77(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                BatmanBlackWhite::JimLeeTheKoker21(val) => val.list_price.clone(),
                BatmanBlackWhite::CarmineInfantinoRobin72(val) => val.list_price.clone(),
                BatmanBlackWhite::DickSprangTheJoker61(val) => val.list_price.clone(),
                BatmanBlackWhite::JasonFabokBatman77(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                BatmanBlackWhite::JimLeeTheKoker21(val) => val.editions.clone(),
                BatmanBlackWhite::CarmineInfantinoRobin72(val) => val.editions.clone(),
                BatmanBlackWhite::DickSprangTheJoker61(val) => val.editions.clone(),
                BatmanBlackWhite::JasonFabokBatman77(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            match &self {
                BatmanBlackWhite::JimLeeTheKoker21(val) => EditionType::FirstAppearance,
                BatmanBlackWhite::CarmineInfantinoRobin72(val) => EditionType::FirstAppearance,
                BatmanBlackWhite::DickSprangTheJoker61(val) => EditionType::None,
                BatmanBlackWhite::JasonFabokBatman77(val) => EditionType::None,
            }
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                BatmanBlackWhite::JimLeeTheKoker21(val) => Rarity::Common,
                BatmanBlackWhite::CarmineInfantinoRobin72(val) => Rarity::UltraRare,
                BatmanBlackWhite::DickSprangTheJoker61(val) => Rarity::Rare,
                BatmanBlackWhite::JasonFabokBatman77(val) => Rarity::Uncommon,
            }
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::BatmanBlackAndWhite
        }

        fn get_series(&self) -> Self::Series {
            Series::BatmanBlackAndWhite(crate::series::common_type::BatmanBlackAndWhite::S2)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                BatmanBlackWhite::JimLeeTheKoker21(val) => val.id.clone(),
                BatmanBlackWhite::CarmineInfantinoRobin72(val) => val.id.clone(),
                BatmanBlackWhite::DickSprangTheJoker61(val) => val.id.clone(),
                BatmanBlackWhite::JasonFabokBatman77(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct JimLeeTheKoker21 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl JimLeeTheKoker21 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct CarmineInfantinoRobin72 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl CarmineInfantinoRobin72 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct DickSprangTheJoker61 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl DickSprangTheJoker61 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct JasonFabokBatman77 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl JasonFabokBatman77 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}

mod s3 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum BatmanBlackWhite {
        DickSprangBatman60(DickSprangBatman60),
        BrianBollandPenguin24(BrianBollandPenguin24),
        AmandaConnerBatman81(AmandaConnerBatman81),
        BruceTimmHareleyQuinn56(BruceTimmHareleyQuinn56),
    }

    impl Object for BatmanBlackWhite {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                BatmanBlackWhite::DickSprangBatman60(val) => val.name.clone(),
                BatmanBlackWhite::BrianBollandPenguin24(val) => val.name.clone(),
                BatmanBlackWhite::AmandaConnerBatman81(val) => val.name.clone(),
                BatmanBlackWhite::BruceTimmHareleyQuinn56(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                BatmanBlackWhite::DickSprangBatman60(val) => val.drop_date.clone(),
                BatmanBlackWhite::BrianBollandPenguin24(val) => val.drop_date.clone(),
                BatmanBlackWhite::AmandaConnerBatman81(val) => val.drop_date.clone(),
                BatmanBlackWhite::BruceTimmHareleyQuinn56(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                BatmanBlackWhite::DickSprangBatman60(val) => val.list_price.clone(),
                BatmanBlackWhite::BrianBollandPenguin24(val) => val.list_price.clone(),
                BatmanBlackWhite::AmandaConnerBatman81(val) => val.list_price.clone(),
                BatmanBlackWhite::BruceTimmHareleyQuinn56(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                BatmanBlackWhite::DickSprangBatman60(val) => val.editions.clone(),
                BatmanBlackWhite::BrianBollandPenguin24(val) => val.editions.clone(),
                BatmanBlackWhite::AmandaConnerBatman81(val) => val.editions.clone(),
                BatmanBlackWhite::BruceTimmHareleyQuinn56(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            match &self {
                BatmanBlackWhite::DickSprangBatman60(val) => EditionType::None,
                BatmanBlackWhite::BrianBollandPenguin24(val) => EditionType::FirstAppearance,
                BatmanBlackWhite::AmandaConnerBatman81(val) => EditionType::None,
                BatmanBlackWhite::BruceTimmHareleyQuinn56(val) => EditionType::FirstAppearance,
            }
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                BatmanBlackWhite::DickSprangBatman60(val) => Rarity::Uncommon,
                BatmanBlackWhite::BrianBollandPenguin24(val) => Rarity::Rare,
                BatmanBlackWhite::AmandaConnerBatman81(val) => Rarity::UltraRare,
                BatmanBlackWhite::BruceTimmHareleyQuinn56(val) => Rarity::Common,
            }
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::BatmanBlackAndWhite
        }

        fn get_series(&self) -> Self::Series {
            Series::BatmanBlackAndWhite(crate::series::common_type::BatmanBlackAndWhite::S3)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                BatmanBlackWhite::DickSprangBatman60(val) => val.id.clone(),
                BatmanBlackWhite::BrianBollandPenguin24(val) => val.id.clone(),
                BatmanBlackWhite::AmandaConnerBatman81(val) => val.id.clone(),
                BatmanBlackWhite::BruceTimmHareleyQuinn56(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct DickSprangBatman60 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl DickSprangBatman60 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct BrianBollandPenguin24 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl BrianBollandPenguin24 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct AmandaConnerBatman81 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl AmandaConnerBatman81 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct BruceTimmHareleyQuinn56 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl BruceTimmHareleyQuinn56 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}

mod s4 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum BatmanBlackWhite {
        JimLeeNightwing79(JimLeeNightwing79),
        JaeLeeBatmen57(JaeLeeBatmen57),
        GaryFrankBatman55(GaryFrankBatman55),
        DarwynCookeBatmen42(DarwynCookeBatmen42),
    }

    impl Object for BatmanBlackWhite {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                BatmanBlackWhite::JimLeeNightwing79(val) => val.name.clone(),
                BatmanBlackWhite::JaeLeeBatmen57(val) => val.name.clone(),
                BatmanBlackWhite::GaryFrankBatman55(val) => val.name.clone(),
                BatmanBlackWhite::DarwynCookeBatmen42(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                BatmanBlackWhite::JimLeeNightwing79(val) => val.drop_date.clone(),
                BatmanBlackWhite::JaeLeeBatmen57(val) => val.drop_date.clone(),
                BatmanBlackWhite::GaryFrankBatman55(val) => val.drop_date.clone(),
                BatmanBlackWhite::DarwynCookeBatmen42(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                BatmanBlackWhite::JimLeeNightwing79(val) => val.list_price.clone(),
                BatmanBlackWhite::JaeLeeBatmen57(val) => val.list_price.clone(),
                BatmanBlackWhite::GaryFrankBatman55(val) => val.list_price.clone(),
                BatmanBlackWhite::DarwynCookeBatmen42(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                BatmanBlackWhite::JimLeeNightwing79(val) => val.editions.clone(),
                BatmanBlackWhite::JaeLeeBatmen57(val) => val.editions.clone(),
                BatmanBlackWhite::GaryFrankBatman55(val) => val.editions.clone(),
                BatmanBlackWhite::DarwynCookeBatmen42(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            match &self {
                BatmanBlackWhite::JimLeeNightwing79(val) => EditionType::FirstAppearance,
                BatmanBlackWhite::JaeLeeBatmen57(val) => EditionType::None,
                BatmanBlackWhite::GaryFrankBatman55(val) => EditionType::None,
                BatmanBlackWhite::DarwynCookeBatmen42(val) => EditionType::None,
            }
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                BatmanBlackWhite::JimLeeNightwing79(val) => Rarity::UltraRare,
                BatmanBlackWhite::JaeLeeBatmen57(val) => Rarity::Rare,
                BatmanBlackWhite::GaryFrankBatman55(val) => Rarity::Uncommon,
                BatmanBlackWhite::DarwynCookeBatmen42(val) => Rarity::Common,
            }
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::BatmanBlackAndWhite
        }

        fn get_series(&self) -> Self::Series {
            Series::BatmanBlackAndWhite(crate::series::common_type::BatmanBlackAndWhite::S4)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                BatmanBlackWhite::JimLeeNightwing79(val) => val.id.clone(),
                BatmanBlackWhite::JaeLeeBatmen57(val) => val.id.clone(),
                BatmanBlackWhite::GaryFrankBatman55(val) => val.id.clone(),
                BatmanBlackWhite::DarwynCookeBatmen42(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct JimLeeNightwing79 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl JimLeeNightwing79 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct JaeLeeBatmen57 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl JaeLeeBatmen57 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct GaryFrankBatman55 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl GaryFrankBatman55 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct DarwynCookeBatmen42 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl DarwynCookeBatmen42 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}

mod s5 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum BatmanBlackWhite {
        Batman89(Batman89),
        Robin76(Robin76),
        Batman71(Batman71),
        Batman06(Batman06),
    }

    impl Object for BatmanBlackWhite {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                BatmanBlackWhite::Batman89(val) => val.name.clone(),
                BatmanBlackWhite::Robin76(val) => val.name.clone(),
                BatmanBlackWhite::Batman71(val) => val.name.clone(),
                BatmanBlackWhite::Batman06(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                BatmanBlackWhite::Batman89(val) => val.drop_date.clone(),
                BatmanBlackWhite::Robin76(val) => val.drop_date.clone(),
                BatmanBlackWhite::Batman71(val) => val.drop_date.clone(),
                BatmanBlackWhite::Batman06(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                BatmanBlackWhite::Batman89(val) => val.list_price.clone(),
                BatmanBlackWhite::Robin76(val) => val.list_price.clone(),
                BatmanBlackWhite::Batman71(val) => val.list_price.clone(),
                BatmanBlackWhite::Batman06(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                BatmanBlackWhite::Batman89(val) => val.editions.clone(),
                BatmanBlackWhite::Robin76(val) => val.editions.clone(),
                BatmanBlackWhite::Batman71(val) => val.editions.clone(),
                BatmanBlackWhite::Batman06(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            match &self {
                BatmanBlackWhite::Batman89(val) => EditionType::FirstAppearance,
                BatmanBlackWhite::Robin76(val) => EditionType::FirstAppearance,
                BatmanBlackWhite::Batman71(val) => EditionType::None,
                BatmanBlackWhite::Batman06(val) => EditionType::None,
            }
        }

        fn get_season(&self) -> Self::Season {
            Season::Season2
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                BatmanBlackWhite::Batman89(val) => Rarity::Common,
                BatmanBlackWhite::Robin76(val) => Rarity::Uncommon,
                BatmanBlackWhite::Batman71(val) => Rarity::Rare,
                BatmanBlackWhite::Batman06(val) => Rarity::UltraRare,
            }
        }

        fn get_license(&self) -> Self::License {
            License::DcDirect
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::BatmanBlackAndWhite
        }

        fn get_series(&self) -> Self::Series {
            Series::BatmanBlackAndWhite(crate::series::common_type::BatmanBlackAndWhite::S5)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                BatmanBlackWhite::Batman89(val) => val.id.clone(),
                BatmanBlackWhite::Robin76(val) => val.id.clone(),
                BatmanBlackWhite::Batman71(val) => val.id.clone(),
                BatmanBlackWhite::Batman06(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    struct Batman89 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Batman89 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    struct Robin76 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Robin76 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    struct Batman71 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Batman71 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    struct Batman06 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Batman06 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
