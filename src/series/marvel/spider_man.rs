use crate::series::common_type::{Brand, License, Rarity, Season, Series};
use crate::series::common_type::{EditionType, ModerMarvel};
use crate::series::Object;
use uuid::Uuid;

impl Object for SpiderMan {
    type Name = String;
    type DropDate = String;
    type ListPrice = f32;
    type Editions = u32;
    type EditionType = EditionType;
    type Season = Season;
    type Rarity = Rarity;
    type License = License;
    type Brand = Brand;
    type Series = Series;
    type Uuid = Uuid;

    fn get_name(&self) -> Self::Name {
        match &self {
            SpiderMan::TheAmazingSpiderMan(val) => val.name.clone(),
            SpiderMan::HanginOut(val) => val.name.clone(),
            SpiderMan::JumpIntoAction(val) => val.name.clone(),
            SpiderMan::Animated(val) => val.name.clone(),
            SpiderMan::UltimateAnimated(val) => val.name.clone(),
        }
    }

    fn get_drop_date(&self) -> Self::DropDate {
        match &self {
            SpiderMan::TheAmazingSpiderMan(val) => val.drop_date.clone(),
            SpiderMan::HanginOut(val) => val.drop_date.clone(),
            SpiderMan::JumpIntoAction(val) => val.drop_date.clone(),
            SpiderMan::Animated(val) => val.drop_date.clone(),
            SpiderMan::UltimateAnimated(val) => val.drop_date.clone(),
        }
    }

    fn get_list_price(&self) -> Self::ListPrice {
        match &self {
            SpiderMan::TheAmazingSpiderMan(val) => val.list_price.clone(),
            SpiderMan::HanginOut(val) => val.list_price.clone(),
            SpiderMan::JumpIntoAction(val) => val.list_price.clone(),
            SpiderMan::Animated(val) => val.list_price.clone(),
            SpiderMan::UltimateAnimated(val) => val.list_price.clone(),
        }
    }

    fn get_editions(&self) -> Self::Editions {
        match &self {
            SpiderMan::TheAmazingSpiderMan(val) => val.editions.clone(),
            SpiderMan::HanginOut(val) => val.editions.clone(),
            SpiderMan::JumpIntoAction(val) => val.editions.clone(),
            SpiderMan::Animated(val) => val.editions.clone(),
            SpiderMan::UltimateAnimated(val) => val.editions.clone(),
        }
    }

    fn get_edition_type(&self) -> Self::EditionType {
        EditionType::FirstAppearance
    }

    fn get_season(&self) -> Self::Season {
        Season::Season2
    }

    fn get_rarity(&self) -> Self::Rarity {
        match &self {
            SpiderMan::TheAmazingSpiderMan(_) => Rarity::Common,
            SpiderMan::HanginOut(_) => Rarity::Uncommon,
            SpiderMan::JumpIntoAction(_) => Rarity::Rare,
            SpiderMan::Animated(_) => Rarity::UltraRare,
            SpiderMan::UltimateAnimated(_) => Rarity::SecretRare,
        }
    }

    fn get_license(&self) -> Self::License {
        License::Marvel
    }

    fn get_brand(&self) -> Self::Brand {
        Brand::SpiderMan
    }

    fn get_series(&self) -> Self::Series {
        Series::ModernMarvel(ModerMarvel::S1SpiderMan)
    }

    fn get_uuid(&self) -> Self::Uuid {
        match &self {
            SpiderMan::TheAmazingSpiderMan(val) => val.id.clone(),
            SpiderMan::HanginOut(val) => val.id.clone(),
            SpiderMan::JumpIntoAction(val) => val.id.clone(),
            SpiderMan::Animated(val) => val.id.clone(),
            SpiderMan::UltimateAnimated(val) => val.id.clone(),
        }
    }
}

#[derive(Debug)]
pub enum SpiderMan {
    TheAmazingSpiderMan(TheAmazingSpiderMan),
    HanginOut(HanginOut),
    JumpIntoAction(JumpIntoAction),
    Animated(Animated),
    UltimateAnimated(UltimateAnimated),
}

#[derive(Debug)]
struct HanginOut {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl HanginOut {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
#[derive(Debug)]
struct JumpIntoAction {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl JumpIntoAction {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
struct UltimateAnimated {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl UltimateAnimated {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
struct Animated {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl Animated {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
struct TheAmazingSpiderMan {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl TheAmazingSpiderMan {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
