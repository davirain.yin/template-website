pub mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug, Clone)]
    pub enum AdventureTime {
        Marceline(Marceline),
        FinnMertens(FinnMertens),
        JakeTheDog(JakeTheDog),
        IceKing(IceKing),
    }
    impl AdventureTime {
        pub fn new() -> Vec<Self> {
            let mut adventuret_time: Vec<AdventureTime> = Vec::new();
            adventuret_time.push(AdventureTime::Marceline(Marceline::new(Uuid::new_v4())));
            adventuret_time.push(AdventureTime::FinnMertens(FinnMertens::new(Uuid::new_v4())));
            adventuret_time.push(AdventureTime::JakeTheDog(JakeTheDog::new(Uuid::new_v4())));
            adventuret_time.push(AdventureTime::IceKing(IceKing::new(Uuid::new_v4())));

            adventuret_time
        }
    }

    #[test]
    fn test_adventure_new() {
        let ret = AdventureTime::new();
        println!("ret = {:#?}", ret);
    }

    impl Object for AdventureTime {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                AdventureTime::Marceline(val) => val.name.clone(),
                AdventureTime::FinnMertens(val) => val.name.clone(),
                AdventureTime::JakeTheDog(val) => val.name.clone(),
                AdventureTime::IceKing(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                AdventureTime::Marceline(val) => val.drop_date.clone(),
                AdventureTime::FinnMertens(val) => val.drop_date.clone(),
                AdventureTime::JakeTheDog(val) => val.drop_date.clone(),
                AdventureTime::IceKing(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                AdventureTime::Marceline(val) => val.list_price.clone(),
                AdventureTime::FinnMertens(val) => val.list_price.clone(),
                AdventureTime::JakeTheDog(val) => val.list_price.clone(),
                AdventureTime::IceKing(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                AdventureTime::Marceline(val) => val.editions.clone(),
                AdventureTime::FinnMertens(val) => val.editions.clone(),
                AdventureTime::JakeTheDog(val) => val.editions.clone(),
                AdventureTime::IceKing(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Self::Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                AdventureTime::Marceline(_) => Self::Rarity::UltraRare,
                AdventureTime::FinnMertens(_) => Self::Rarity::Common,
                AdventureTime::JakeTheDog(_) => Self::Rarity::Uncommon,
                AdventureTime::IceKing(_) => Self::Rarity::Rare,
            }
        }

        fn get_license(&self) -> Self::License {
            Self::License::CartoonNetwork
        }

        fn get_brand(&self) -> Self::Brand {
            Self::Brand::AdventureTime
        }

        fn get_series(&self) -> Self::Series {
            Self::Series::AdventureTime(crate::series::common_type::AdventureTime::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                AdventureTime::Marceline(val) => val.id.clone(),
                AdventureTime::FinnMertens(val) => val.id.clone(),
                AdventureTime::JakeTheDog(val) => val.id.clone(),
                AdventureTime::IceKing(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug, Clone)]
    pub struct Marceline {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Marceline {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Marceline".to_string(),
                drop_date: "20 Oct 2020".to_string(),
                list_price: 15.99f32,
                editions: 1750u32,
            }
        }
    }

    #[test]
    fn test_marceline_new() {
        let temp_obj = Marceline::new(Uuid::new_v4());
        println!("{:?}", temp_obj);
    }

    #[derive(Debug, Clone)]
    pub struct FinnMertens {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl FinnMertens {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Finn Mertens".to_string(),
                drop_date: "20 Oct 2020".to_string(),
                list_price: 12.99f32,
                editions: 7500u32,
            }
        }
    }

    #[derive(Debug, Clone)]
    pub struct JakeTheDog {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl JakeTheDog {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Jake The Dog".to_string(),
                drop_date: "20 Oct 2020".to_string(),
                list_price: 14.99f32,
                editions: 5500u32,
            }
        }
    }

    #[derive(Debug, Clone)]
    pub struct IceKing {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl IceKing {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Ice King".to_string(),
                drop_date: "20 Oct 2020".to_string(),
                list_price: 14.99f32,
                editions: 3250u32,
            }
        }
    }
}

pub mod s2 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum AdventureTime {
        LumpySpacePrincess(LumpySpacePrincess),
        LadyRainicorn(LadyRainicorn),
        PrincessBubblegum(PrincessBubblegum),
        BMO(BMO),
    }

    impl Object for AdventureTime {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                AdventureTime::LumpySpacePrincess(val) => val.name.clone(),
                AdventureTime::LadyRainicorn(val) => val.name.clone(),
                AdventureTime::PrincessBubblegum(val) => val.name.clone(),
                AdventureTime::BMO(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                AdventureTime::LumpySpacePrincess(val) => val.drop_date.clone(),
                AdventureTime::LadyRainicorn(val) => val.drop_date.clone(),
                AdventureTime::PrincessBubblegum(val) => val.drop_date.clone(),
                AdventureTime::BMO(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                AdventureTime::LumpySpacePrincess(val) => val.list_price.clone(),
                AdventureTime::LadyRainicorn(val) => val.list_price.clone(),
                AdventureTime::PrincessBubblegum(val) => val.list_price.clone(),
                AdventureTime::BMO(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                AdventureTime::LumpySpacePrincess(val) => val.editions.clone(),
                AdventureTime::LadyRainicorn(val) => val.editions.clone(),
                AdventureTime::PrincessBubblegum(val) => val.editions.clone(),
                AdventureTime::BMO(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Self::Season::Season2
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                AdventureTime::LumpySpacePrincess(val) => Self::Rarity::Rare,
                AdventureTime::LadyRainicorn(val) => Self::Rarity::UltraRare,
                AdventureTime::PrincessBubblegum(val) => Self::Rarity::Uncommon,
                AdventureTime::BMO(val) => Self::Rarity::Common,
            }
        }

        fn get_license(&self) -> Self::License {
            Self::License::CartoonNetwork
        }

        fn get_brand(&self) -> Self::Brand {
            Self::Brand::AdventureTime
        }

        fn get_series(&self) -> Self::Series {
            Self::Series::AdventureTime(crate::series::common_type::AdventureTime::S2)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                AdventureTime::LumpySpacePrincess(val) => val.id.clone(),
                AdventureTime::LadyRainicorn(val) => val.id.clone(),
                AdventureTime::PrincessBubblegum(val) => val.id.clone(),
                AdventureTime::BMO(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct LumpySpacePrincess {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl LumpySpacePrincess {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Lumpy Space Princess".to_string(),
                drop_date: "24 Dec 2020".to_string(),
                list_price: 15.99f32,
                editions: 3250u32,
            }
        }
    }

    #[derive(Debug)]
    pub struct LadyRainicorn {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl LadyRainicorn {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Lady Rainicorn".to_string(),
                drop_date: "24 Dec 2020".to_string(),
                list_price: 17.99,
                editions: 1750,
            }
        }
    }

    #[derive(Debug)]
    pub struct PrincessBubblegum {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl PrincessBubblegum {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Princess Bubblegum".to_string(),
                drop_date: "24 Dec 2020".to_string(),
                list_price: 14.99,
                editions: 5500,
            }
        }
    }

    #[derive(Debug)]
    pub struct BMO {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl BMO {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "BMO".to_string(),
                drop_date: "24 Dec 2020".to_string(),
                list_price: 14.99,
                editions: 7500,
            }
        }
    }
}
