mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum PowerpuffGirls {
        MayorOfTownsville(MayorOfTownsville),
        Buttercup(Buttercup),
        Blossom(Blossom),
        MojoJojo(MojoJojo),
    }

    impl Object for PowerpuffGirls {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                PowerpuffGirls::MayorOfTownsville(val) => val.name.clone(),
                PowerpuffGirls::Buttercup(val) => val.name.clone(),
                PowerpuffGirls::Blossom(val) => val.name.clone(),
                PowerpuffGirls::MojoJojo(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                PowerpuffGirls::MayorOfTownsville(val) => val.drop_date.clone(),
                PowerpuffGirls::Buttercup(val) => val.drop_date.clone(),
                PowerpuffGirls::Blossom(val) => val.drop_date.clone(),
                PowerpuffGirls::MojoJojo(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                PowerpuffGirls::MayorOfTownsville(val) => val.list_price.clone(),
                PowerpuffGirls::Buttercup(val) => val.list_price.clone(),
                PowerpuffGirls::Blossom(val) => val.list_price.clone(),
                PowerpuffGirls::MojoJojo(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                PowerpuffGirls::MayorOfTownsville(val) => val.editions.clone(),
                PowerpuffGirls::Buttercup(val) => val.editions.clone(),
                PowerpuffGirls::Blossom(val) => val.editions.clone(),
                PowerpuffGirls::MojoJojo(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Self::Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                PowerpuffGirls::MayorOfTownsville(val) => Self::Rarity::UltraRare,
                PowerpuffGirls::Buttercup(val) => Self::Rarity::Uncommon,
                PowerpuffGirls::Blossom(val) => Self::Rarity::Common,
                PowerpuffGirls::MojoJojo(val) => Self::Rarity::Rare,
            }
        }

        fn get_license(&self) -> Self::License {
            Self::License::CartoonNetwork
        }

        fn get_brand(&self) -> Self::Brand {
            Self::Brand::PowerpuffGirls
        }

        fn get_series(&self) -> Self::Series {
            Self::Series::PowerpuffGirls(crate::series::common_type::PowerpuffGirls::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                PowerpuffGirls::MayorOfTownsville(val) => val.id.clone(),
                PowerpuffGirls::Buttercup(val) => val.id.clone(),
                PowerpuffGirls::Blossom(val) => val.id.clone(),
                PowerpuffGirls::MojoJojo(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct MayorOfTownsville {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl MayorOfTownsville {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Mayor Of Townsville".to_string(),
                drop_date: "04 Nov 2020".to_string(),
                list_price: 15.99,
                editions: 1750,
            }
        }
    }

    #[derive(Debug)]
    pub struct Buttercup {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Buttercup {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Buttercup".to_string(),
                drop_date: "04 Nov 2020".to_string(),
                list_price: 12.99,
                editions: 4000,
            }
        }
    }

    #[derive(Debug)]
    pub struct Blossom {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Blossom {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Blossom".to_string(),
                drop_date: "04 Nov 2020".to_string(),
                list_price: 12.99,
                editions: 5000,
            }
        }
    }

    #[derive(Debug)]
    pub struct MojoJojo {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl MojoJojo {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Mojo Jojo".to_string(),
                drop_date: "04 Nov 2020".to_string(),
                list_price: 14.99,
                editions: 3000,
            }
        }
    }
}

mod s2 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum PowerpuffGirls {
        Donny(Donny),
        Bubbles(Bubbles),
        ProfessorUtonium(ProfessorUtonium),
        HIM(HIM),
    }

    impl Object for PowerpuffGirls {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                PowerpuffGirls::Donny(val) => val.name.clone(),
                PowerpuffGirls::Bubbles(val) => val.name.clone(),
                PowerpuffGirls::ProfessorUtonium(val) => val.name.clone(),
                PowerpuffGirls::HIM(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                PowerpuffGirls::Donny(val) => val.drop_date.clone(),
                PowerpuffGirls::Bubbles(val) => val.drop_date.clone(),
                PowerpuffGirls::ProfessorUtonium(val) => val.drop_date.clone(),
                PowerpuffGirls::HIM(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                PowerpuffGirls::Donny(val) => val.list_price.clone(),
                PowerpuffGirls::Bubbles(val) => val.list_price.clone(),
                PowerpuffGirls::ProfessorUtonium(val) => val.list_price.clone(),
                PowerpuffGirls::HIM(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                PowerpuffGirls::Donny(val) => val.editions.clone(),
                PowerpuffGirls::Bubbles(val) => val.editions.clone(),
                PowerpuffGirls::ProfessorUtonium(val) => val.editions.clone(),
                PowerpuffGirls::HIM(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Self::Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                PowerpuffGirls::Donny(val) => Self::Rarity::UltraRare,
                PowerpuffGirls::Bubbles(val) => Self::Rarity::Common,
                PowerpuffGirls::ProfessorUtonium(val) => Self::Rarity::Uncommon,
                PowerpuffGirls::HIM(val) => Self::Rarity::Rare,
            }
        }

        fn get_license(&self) -> Self::License {
            Self::License::CartoonNetwork
        }

        fn get_brand(&self) -> Self::Brand {
            Self::Brand::PowerpuffGirls
        }

        fn get_series(&self) -> Self::Series {
            Self::Series::PowerpuffGirls(crate::series::common_type::PowerpuffGirls::S2)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                PowerpuffGirls::Donny(val) => val.id.clone(),
                PowerpuffGirls::Bubbles(val) => val.id.clone(),
                PowerpuffGirls::ProfessorUtonium(val) => val.id.clone(),
                PowerpuffGirls::HIM(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct Donny {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Donny {
        pub fn new(id: Uuid) -> Self {
            Self {
                id,
                name: "Donny".to_string(),
                drop_date: "07 Jan 2021".to_string(),
                list_price: 16.99,
                editions: 150,
            }
        }
    }

    #[derive(Debug)]
    pub struct Bubbles {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Bubbles {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct ProfessorUtonium {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl ProfessorUtonium {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct HIM {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl HIM {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
