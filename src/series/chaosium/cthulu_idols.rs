use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
use crate::series::Object;
use uuid::Uuid;

impl Object for CthuluIdols {
    type Name = String;
    type DropDate = String;
    type ListPrice = f32;
    type Editions = u32;
    type EditionType = EditionType;
    type Season = Season;
    type Rarity = Rarity;
    type License = License;
    type Brand = Brand;
    type Series = Series;
    type Uuid = Uuid;

    fn get_name(&self) -> Self::Name {
        match &self {
            CthuluIdols::Necronomicon(val) => val.name.clone(),
            CthuluIdols::Jade(val) => val.name.clone(),
            CthuluIdols::Gold(val) => val.name.clone(),
            CthuluIdols::Bronze(val) => val.name.clone(),
        }
    }

    fn get_drop_date(&self) -> Self::DropDate {
        match &self {
            CthuluIdols::Necronomicon(val) => val.drop_date.clone(),
            CthuluIdols::Jade(val) => val.drop_date.clone(),
            CthuluIdols::Gold(val) => val.drop_date.clone(),
            CthuluIdols::Bronze(val) => val.drop_date.clone(),
        }
    }

    fn get_list_price(&self) -> Self::ListPrice {
        match &self {
            CthuluIdols::Necronomicon(val) => val.list_price.clone(),
            CthuluIdols::Jade(val) => val.list_price.clone(),
            CthuluIdols::Gold(val) => val.list_price.clone(),
            CthuluIdols::Bronze(val) => val.list_price.clone(),
        }
    }

    fn get_editions(&self) -> Self::Editions {
        match &self {
            CthuluIdols::Necronomicon(val) => val.editions.clone(),
            CthuluIdols::Jade(val) => val.editions.clone(),
            CthuluIdols::Gold(val) => val.editions.clone(),
            CthuluIdols::Bronze(val) => val.editions.clone(),
        }
    }

    fn get_edition_type(&self) -> Self::EditionType {
        EditionType::ConExclusive
    }

    fn get_season(&self) -> Self::Season {
        Season::Season2
    }

    fn get_rarity(&self) -> Self::Rarity {
        Rarity::Rare
    }

    fn get_license(&self) -> Self::License {
        License::Chaosium
    }

    fn get_brand(&self) -> Self::Brand {
        Brand::CallOfCthulhu
    }

    fn get_series(&self) -> Self::Series {
        Series::CthuluIdols
    }

    fn get_uuid(&self) -> Self::Uuid {
        match &self {
            CthuluIdols::Necronomicon(val) => val.id.clone(),
            CthuluIdols::Jade(val) => val.id.clone(),
            CthuluIdols::Gold(val) => val.id.clone(),
            CthuluIdols::Bronze(val) => val.id.clone(),
        }
    }
}

#[derive(Debug)]
pub enum CthuluIdols {
    Necronomicon(Necronomicon),
    Bronze(Bronze),
    Gold(Gold),
    Jade(Jade),
}

impl CthuluIdols {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct Necronomicon {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl Necronomicon {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct Bronze {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl Bronze {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct Gold {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl Gold {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct Jade {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl Jade {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
