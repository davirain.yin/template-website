mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum Mondo {
        Jaws(Jaws),
        BackToTheFuture(BackToTheFuture),
        BillTedBogusJourney(BillTedBogusJourney),
        BillTedExcellentAdventure(BillTedExcellentAdventure),
    }

    impl Object for Mondo {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                Mondo::Jaws(val) => val.name.clone(),
                Mondo::BackToTheFuture(val) => val.name.clone(),
                Mondo::BillTedBogusJourney(val) => val.name.clone(),
                Mondo::BillTedExcellentAdventure(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                Mondo::Jaws(val) => val.drop_date.clone(),
                Mondo::BackToTheFuture(val) => val.drop_date.clone(),
                Mondo::BillTedBogusJourney(val) => val.drop_date.clone(),
                Mondo::BillTedExcellentAdventure(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                Mondo::Jaws(val) => val.list_price.clone(),
                Mondo::BackToTheFuture(val) => val.list_price.clone(),
                Mondo::BillTedBogusJourney(val) => val.list_price.clone(),
                Mondo::BillTedExcellentAdventure(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                Mondo::Jaws(val) => val.editions.clone(),
                Mondo::BackToTheFuture(val) => val.editions.clone(),
                Mondo::BillTedBogusJourney(val) => val.editions.clone(),
                Mondo::BillTedExcellentAdventure(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::ConExclusive
        }

        fn get_season(&self) -> Self::Season {
            Self::Season::Season2
        }

        fn get_rarity(&self) -> Self::Rarity {
            Self::Rarity::Rare
        }

        fn get_license(&self) -> Self::License {
            Self::License::Mondo
        }

        fn get_brand(&self) -> Self::Brand {
            Self::Brand::ConExClusivePrints
        }

        fn get_series(&self) -> Self::Series {
            Self::Series::Mondo(crate::series::common_type::Mondo::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                Mondo::Jaws(val) => val.id.clone(),
                Mondo::BackToTheFuture(val) => val.id.clone(),
                Mondo::BillTedBogusJourney(val) => val.id.clone(),
                Mondo::BillTedExcellentAdventure(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct Jaws {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Jaws {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct BackToTheFuture {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl BackToTheFuture {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct BillTedBogusJourney {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl BillTedBogusJourney {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct BillTedExcellentAdventure {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl BillTedExcellentAdventure {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
