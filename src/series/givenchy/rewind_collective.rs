use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
use crate::series::Object;
use uuid::Uuid;

#[derive(Debug)]
pub enum GivenchyRewindCollective {
    RewindCollectiveLogo(RewindCollectiveLogo),
    PrideGivenchyBeauty(PrideGivenchyBeauty),
    HouseOfCardsAmeliaEarhart(HouseOfCardsAmeliaEarhart),
    HoseOfCardsCleopatra(HoseOfCardsCleopatra),
}

impl Object for GivenchyRewindCollective {
    type Name = String;
    type DropDate = String;
    type ListPrice = f32;
    type Editions = u32;
    type EditionType = EditionType;
    type Season = Season;
    type Rarity = Rarity;
    type License = License;
    type Brand = Brand;
    type Series = Series;
    type Uuid = Uuid;

    fn get_name(&self) -> Self::Name {
        match &self {
            GivenchyRewindCollective::RewindCollectiveLogo(val) => val.name.clone(),
            GivenchyRewindCollective::PrideGivenchyBeauty(val) => val.name.clone(),
            GivenchyRewindCollective::HouseOfCardsAmeliaEarhart(val) => val.name.clone(),
            GivenchyRewindCollective::HoseOfCardsCleopatra(val) => val.name.clone(),
        }
    }

    fn get_drop_date(&self) -> Self::DropDate {
        match &self {
            GivenchyRewindCollective::RewindCollectiveLogo(val) => val.drop_date.clone(),
            GivenchyRewindCollective::PrideGivenchyBeauty(val) => val.drop_date.clone(),
            GivenchyRewindCollective::HouseOfCardsAmeliaEarhart(val) => val.drop_date.clone(),
            GivenchyRewindCollective::HoseOfCardsCleopatra(val) => val.drop_date.clone(),
        }
    }

    fn get_list_price(&self) -> Self::ListPrice {
        match &self {
            GivenchyRewindCollective::RewindCollectiveLogo(val) => val.list_price.clone(),
            GivenchyRewindCollective::PrideGivenchyBeauty(val) => val.list_price.clone(),
            GivenchyRewindCollective::HouseOfCardsAmeliaEarhart(val) => val.list_price.clone(),
            GivenchyRewindCollective::HoseOfCardsCleopatra(val) => val.list_price.clone(),
        }
    }

    fn get_editions(&self) -> Self::Editions {
        match &self {
            GivenchyRewindCollective::RewindCollectiveLogo(val) => val.editions.clone(),
            GivenchyRewindCollective::PrideGivenchyBeauty(val) => val.editions.clone(),
            GivenchyRewindCollective::HouseOfCardsAmeliaEarhart(val) => val.editions.clone(),
            GivenchyRewindCollective::HoseOfCardsCleopatra(val) => val.editions.clone(),
        }
    }

    fn get_edition_type(&self) -> Self::EditionType {
        EditionType::FirstEdition
    }

    fn get_season(&self) -> Self::Season {
        Season::Season2
    }

    fn get_rarity(&self) -> Self::Rarity {
        match &self {
            GivenchyRewindCollective::RewindCollectiveLogo(val) => Rarity::Uncommon,
            GivenchyRewindCollective::PrideGivenchyBeauty(val) => Rarity::Rare,
            GivenchyRewindCollective::HouseOfCardsAmeliaEarhart(val) => Rarity::UltraRare,
            GivenchyRewindCollective::HoseOfCardsCleopatra(val) => Rarity::UltraRare,
        }
    }

    fn get_license(&self) -> Self::License {
        License::Givenchy
    }

    fn get_brand(&self) -> Self::Brand {
        Brand::GivenchyRewindCollective
    }

    fn get_series(&self) -> Self::Series {
        Series::GivenchyRewindCollective
    }

    fn get_uuid(&self) -> Self::Uuid {
        match &self {
            GivenchyRewindCollective::RewindCollectiveLogo(val) => val.id.clone(),
            GivenchyRewindCollective::PrideGivenchyBeauty(val) => val.id.clone(),
            GivenchyRewindCollective::HouseOfCardsAmeliaEarhart(val) => val.id.clone(),
            GivenchyRewindCollective::HoseOfCardsCleopatra(val) => val.id.clone(),
        }
    }
}

#[derive(Debug)]
pub struct RewindCollectiveLogo {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl RewindCollectiveLogo {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct PrideGivenchyBeauty {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl PrideGivenchyBeauty {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct HouseOfCardsAmeliaEarhart {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl HouseOfCardsAmeliaEarhart {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct HoseOfCardsCleopatra {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl HoseOfCardsCleopatra {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
