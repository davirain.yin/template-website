use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
use crate::series::Object;
use uuid::Uuid;

#[derive(Debug)]
pub enum Ecto1 {
    InteractiveUltraRare(InteractiveUltraRare),
    InteractiveCommon(InteractiveCommon),
    StaticRare(StaticRare),
}

impl Object for Ecto1 {
    type Name = String;
    type DropDate = String;
    type ListPrice = f32;
    type Editions = u32;
    type EditionType = EditionType;
    type Season = Season;
    type Rarity = Rarity;
    type License = License;
    type Brand = Brand;
    type Series = Series;
    type Uuid = Uuid;

    fn get_name(&self) -> Self::Name {
        match &self {
            Ecto1::InteractiveUltraRare(val) => val.name.clone(),
            Ecto1::InteractiveCommon(val) => val.name.clone(),
            Ecto1::StaticRare(val) => val.name.clone(),
        }
    }

    fn get_drop_date(&self) -> Self::DropDate {
        match &self {
            Ecto1::InteractiveUltraRare(val) => val.drop_date.clone(),
            Ecto1::InteractiveCommon(val) => val.drop_date.clone(),
            Ecto1::StaticRare(val) => val.drop_date.clone(),
        }
    }

    fn get_list_price(&self) -> Self::ListPrice {
        match &self {
            Ecto1::InteractiveUltraRare(val) => val.list_price.clone(),
            Ecto1::InteractiveCommon(val) => val.list_price.clone(),
            Ecto1::StaticRare(val) => val.list_price.clone(),
        }
    }

    fn get_editions(&self) -> Self::Editions {
        match &self {
            Ecto1::InteractiveUltraRare(val) => val.editions.clone(),
            Ecto1::InteractiveCommon(val) => val.editions.clone(),
            Ecto1::StaticRare(val) => val.editions.clone(),
        }
    }

    fn get_edition_type(&self) -> Self::EditionType {
        EditionType::FirstAppearance
    }

    fn get_season(&self) -> Self::Season {
        Season::Season2
    }

    fn get_rarity(&self) -> Self::Rarity {
        match &self {
            Ecto1::InteractiveUltraRare(val) => Rarity::UltraRare,
            Ecto1::InteractiveCommon(val) => Rarity::Common,
            Ecto1::StaticRare(val) => Rarity::Rare,
        }
    }

    fn get_license(&self) -> Self::License {
        License::Ghostbusters
    }

    fn get_brand(&self) -> Self::Brand {
        Brand::Ghostbusters(crate::series::common_type::GhostbustersBand::I)
    }

    fn get_series(&self) -> Self::Series {
        Series::Ecto1
    }

    fn get_uuid(&self) -> Self::Uuid {
        match &self {
            Ecto1::InteractiveUltraRare(val) => val.id.clone(),
            Ecto1::InteractiveCommon(val) => val.id.clone(),
            Ecto1::StaticRare(val) => val.id.clone(),
        }
    }
}

#[derive(Debug)]
pub struct InteractiveUltraRare {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl InteractiveUltraRare {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct InteractiveCommon {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl InteractiveCommon {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct StaticRare {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl StaticRare {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
