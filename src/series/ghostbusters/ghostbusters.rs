mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum Ghostbusters {
        Platinum(Platinum),
        Logo(Logo),
        GhostTrap(GhostTrap),
        StayPuftMarshmallowMan(StayPuftMarshmallowMan),
        Slimer(Slimer),
    }

    impl Object for Ghostbusters {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                Ghostbusters::Platinum(val) => val.name.clone(),
                Ghostbusters::Logo(val) => val.name.clone(),
                Ghostbusters::GhostTrap(val) => val.name.clone(),
                Ghostbusters::StayPuftMarshmallowMan(val) => val.name.clone(),
                Ghostbusters::Slimer(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                Ghostbusters::Platinum(val) => val.drop_date.clone(),
                Ghostbusters::Logo(val) => val.drop_date.clone(),
                Ghostbusters::GhostTrap(val) => val.drop_date.clone(),
                Ghostbusters::StayPuftMarshmallowMan(val) => val.drop_date.clone(),
                Ghostbusters::Slimer(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                Ghostbusters::Platinum(val) => val.list_price.clone(),
                Ghostbusters::Logo(val) => val.list_price.clone(),
                Ghostbusters::GhostTrap(val) => val.list_price.clone(),
                Ghostbusters::StayPuftMarshmallowMan(val) => val.list_price.clone(),
                Ghostbusters::Slimer(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                Ghostbusters::Platinum(val) => val.editions.clone(),
                Ghostbusters::Logo(val) => val.editions.clone(),
                Ghostbusters::GhostTrap(val) => val.editions.clone(),
                Ghostbusters::StayPuftMarshmallowMan(val) => val.editions.clone(),
                Ghostbusters::Slimer(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            match &self {
                Ghostbusters::Platinum(val) => EditionType::FirstEdition,
                Ghostbusters::Logo(val) => EditionType::FirstAppearance,
                Ghostbusters::GhostTrap(val) => EditionType::FirstAppearance,
                Ghostbusters::StayPuftMarshmallowMan(val) => EditionType::FirstAppearance,
                Ghostbusters::Slimer(val) => EditionType::FirstAppearance,
            }
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                Ghostbusters::Platinum(val) => Rarity::SecretRare,
                Ghostbusters::Logo(val) => Rarity::Rare,
                Ghostbusters::GhostTrap(val) => Rarity::UltraRare,
                Ghostbusters::StayPuftMarshmallowMan(val) => Rarity::Uncommon,
                Ghostbusters::Slimer(val) => Rarity::Common,
            }
        }

        fn get_license(&self) -> Self::License {
            License::Ghostbusters
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::Ghostbusters(crate::series::common_type::GhostbustersBand::I)
        }

        fn get_series(&self) -> Self::Series {
            Series::GhostbustersI(crate::series::common_type::GhostbustersISeries::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                Ghostbusters::Platinum(val) => val.id.clone(),
                Ghostbusters::Logo(val) => val.id.clone(),
                Ghostbusters::GhostTrap(val) => val.id.clone(),
                Ghostbusters::StayPuftMarshmallowMan(val) => val.id.clone(),
                Ghostbusters::Slimer(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct Platinum {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Platinum {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Logo {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Logo {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct GhostTrap {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl GhostTrap {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct StayPuftMarshmallowMan {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl StayPuftMarshmallowMan {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Slimer {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Slimer {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
