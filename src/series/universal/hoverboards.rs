use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
use crate::series::Object;
use uuid::Uuid;

#[derive(Debug)]
pub enum Hoverboards {
    Martys(Martys),
    Pitbull(Pitbull),
    QuestionMark(QuestionMark),
    NoTech(NoTech),
    RisingSun(RisingSun),
}

impl Object for Hoverboards {
    type Name = String;
    type DropDate = String;
    type ListPrice = f32;
    type Editions = u32;
    type EditionType = EditionType;
    type Season = Season;
    type Rarity = Rarity;
    type License = License;
    type Brand = Brand;
    type Series = Series;
    type Uuid = Uuid;

    fn get_name(&self) -> Self::Name {
        match &self {
            Hoverboards::Martys(val) => val.name.clone(),
            Hoverboards::Pitbull(val) => val.name.clone(),
            Hoverboards::QuestionMark(val) => val.name.clone(),
            Hoverboards::NoTech(val) => val.name.clone(),
            Hoverboards::RisingSun(val) => val.name.clone(),
        }
    }

    fn get_drop_date(&self) -> Self::DropDate {
        match &self {
            Hoverboards::Martys(val) => val.drop_date.clone(),
            Hoverboards::Pitbull(val) => val.drop_date.clone(),
            Hoverboards::QuestionMark(val) => val.drop_date.clone(),
            Hoverboards::NoTech(val) => val.drop_date.clone(),
            Hoverboards::RisingSun(val) => val.drop_date.clone(),
        }
    }

    fn get_list_price(&self) -> Self::ListPrice {
        match &self {
            Hoverboards::Martys(val) => val.list_price.clone(),
            Hoverboards::Pitbull(val) => val.list_price.clone(),
            Hoverboards::QuestionMark(val) => val.list_price.clone(),
            Hoverboards::NoTech(val) => val.list_price.clone(),
            Hoverboards::RisingSun(val) => val.list_price.clone(),
        }
    }

    fn get_editions(&self) -> Self::Editions {
        match &self {
            Hoverboards::Martys(val) => val.editions.clone(),
            Hoverboards::Pitbull(val) => val.editions.clone(),
            Hoverboards::QuestionMark(val) => val.editions.clone(),
            Hoverboards::NoTech(val) => val.editions.clone(),
            Hoverboards::RisingSun(val) => val.editions.clone(),
        }
    }

    fn get_edition_type(&self) -> Self::EditionType {
        EditionType::FirstAppearance
    }

    fn get_season(&self) -> Self::Season {
        Season::Season2
    }

    fn get_rarity(&self) -> Self::Rarity {
        match &self {
            Hoverboards::Martys(val) => Rarity::Common,
            Hoverboards::Pitbull(val) => Rarity::UltraRare,
            Hoverboards::QuestionMark(val) => Rarity::Rare,
            Hoverboards::NoTech(val) => Rarity::Common,
            Hoverboards::RisingSun(val) => Rarity::Uncommon,
        }
    }

    fn get_license(&self) -> Self::License {
        License::Universal
    }

    fn get_brand(&self) -> Self::Brand {
        Brand::BackToTheFuture
    }

    fn get_series(&self) -> Self::Series {
        Series::Hoverboards
    }

    fn get_uuid(&self) -> Self::Uuid {
        match &self {
            Hoverboards::Martys(val) => val.id.clone(),
            Hoverboards::Pitbull(val) => val.id.clone(),
            Hoverboards::QuestionMark(val) => val.id.clone(),
            Hoverboards::NoTech(val) => val.id.clone(),
            Hoverboards::RisingSun(val) => val.id.clone(),
        }
    }
}

#[derive(Debug)]
pub struct Martys {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl Martys {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct Pitbull {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl Pitbull {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
#[derive(Debug)]
pub struct QuestionMark {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl QuestionMark {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct NoTech {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl NoTech {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct RisingSun {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl RisingSun {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
