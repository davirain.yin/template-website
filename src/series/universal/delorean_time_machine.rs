use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
use crate::series::Object;
use uuid::Uuid;

#[derive(Debug)]
pub enum DeloreanTimeMachine {
    InteractiveCommon(InteractiveCommon),
    StaticRare(StaticRare),
    InteractiveUltraRare(InteractiveUltraRare),
}

impl Object for DeloreanTimeMachine {
    type Name = String;
    type DropDate = String;
    type ListPrice = f32;
    type Editions = u32;
    type EditionType = EditionType;
    type Season = Season;
    type Rarity = Rarity;
    type License = License;
    type Brand = Brand;
    type Series = Series;
    type Uuid = Uuid;

    fn get_name(&self) -> Self::Name {
        match &self {
            DeloreanTimeMachine::InteractiveCommon(val) => val.name.clone(),
            DeloreanTimeMachine::StaticRare(val) => val.name.clone(),
            DeloreanTimeMachine::InteractiveUltraRare(val) => val.name.clone(),
        }
    }

    fn get_drop_date(&self) -> Self::DropDate {
        match &self {
            DeloreanTimeMachine::InteractiveCommon(val) => val.drop_date.clone(),
            DeloreanTimeMachine::StaticRare(val) => val.drop_date.clone(),
            DeloreanTimeMachine::InteractiveUltraRare(val) => val.drop_date.clone(),
        }
    }

    fn get_list_price(&self) -> Self::ListPrice {
        match &self {
            DeloreanTimeMachine::InteractiveCommon(val) => val.list_price.clone(),
            DeloreanTimeMachine::StaticRare(val) => val.list_price.clone(),
            DeloreanTimeMachine::InteractiveUltraRare(val) => val.list_price.clone(),
        }
    }

    fn get_editions(&self) -> Self::Editions {
        match &self {
            DeloreanTimeMachine::InteractiveCommon(val) => val.editions.clone(),
            DeloreanTimeMachine::StaticRare(val) => val.editions.clone(),
            DeloreanTimeMachine::InteractiveUltraRare(val) => val.editions.clone(),
        }
    }

    fn get_edition_type(&self) -> Self::EditionType {
        EditionType::FirstAppearance
    }

    fn get_season(&self) -> Self::Season {
        Season::Season1
    }

    fn get_rarity(&self) -> Self::Rarity {
        match &self {
            DeloreanTimeMachine::InteractiveCommon(val) => Rarity::Common,
            DeloreanTimeMachine::StaticRare(val) => Rarity::Rare,
            DeloreanTimeMachine::InteractiveUltraRare(val) => Rarity::UltraRare,
        }
    }

    fn get_license(&self) -> Self::License {
        License::Universal
    }

    fn get_brand(&self) -> Self::Brand {
        Brand::BackToTheFuture
    }

    fn get_series(&self) -> Self::Series {
        Series::DeLoreanTimeMachine
    }

    fn get_uuid(&self) -> Self::Uuid {
        match &self {
            DeloreanTimeMachine::InteractiveCommon(val) => val.id.clone(),
            DeloreanTimeMachine::StaticRare(val) => val.id.clone(),
            DeloreanTimeMachine::InteractiveUltraRare(val) => val.id.clone(),
        }
    }
}

#[derive(Debug)]
pub struct InteractiveCommon {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl InteractiveCommon {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
#[derive(Debug)]
pub struct StaticRare {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl StaticRare {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct InteractiveUltraRare {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl InteractiveUltraRare {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
