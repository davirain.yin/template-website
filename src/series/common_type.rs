use super::SeriesContent;
use std::fmt::{self, Display, Formatter};

#[derive(Debug)]
pub enum EditionType {
    FirstEdition,
    FirstAppearance,
    ConExclusive,
    None,
}

impl Display for EditionType {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            EditionType::FirstEdition => write!(f, "First Edition"),
            EditionType::FirstAppearance => write!(f, "First Appearance"),
            EditionType::ConExclusive => write!(f, "Con Exclusive"),
            EditionType::None => write!(f, "None"),
        }
    }
}

#[derive(Debug)]
pub enum Season {
    Season1,
    Season2,
}

impl Display for Season {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Season::Season1 => write!(f, "Season1"),
            Season::Season2 => write!(f, "Season2"),
        }
    }
}

#[derive(Debug)]
pub enum Rarity {
    Common,
    Uncommon,
    Rare,
    UltraRare,
    SecretRare,
}

impl Display for Rarity {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Rarity::Common => write!(f, "Common"),
            Rarity::Uncommon => write!(f, "Uncommon"),
            Rarity::Rare => write!(f, "Rare"),
            Rarity::UltraRare => write!(f, "Ultra Rare"),
            Rarity::SecretRare => write!(f, "Secret Rare"),
        }
    }
}

#[derive(Debug)]
pub enum License {
    Marvel,
    ToyTokyo,
    Chaosium,
    JermaineRogers,
    Mondo,
    Ghostbusters,
    Universal,
    FrankKozik,
    DcDirect,
    Givenchy,
    RonEnglish,
    Tokidoki,
    Cryptozoic,
    Tsuburaya,
    CartoonNetwork,
}

impl Display for License {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            License::Marvel => write!(f, "Marvel"),
            License::ToyTokyo => write!(f, "Toy Tokyo"),
            License::Chaosium => write!(f, "Chaosium"),
            License::JermaineRogers => write!(f, "Jermaine Rogers"),
            License::Mondo => write!(f, "Mondo"),
            License::Ghostbusters => write!(f, "Ghostbusters"),
            License::Universal => write!(f, "Universal"),
            License::FrankKozik => write!(f, "Frank Kozik"),
            License::DcDirect => write!(f, "Dc Direct"),
            License::Givenchy => write!(f, "GIVENCHY"),
            License::RonEnglish => write!(f, "Ron English"),
            License::Tokidoki => write!(f, "Tokidoki"),
            License::Cryptozoic => write!(f, "Cryptozoic"),
            License::Tsuburaya => write!(f, "Tsuburaya"),
            License::CartoonNetwork => write!(f, "Cartoon Network"),
        }
    }
}

#[derive(Debug)]
pub enum Brand {
    SpiderMan,
    ToyTokyoAndJermaineRogers,
    CallOfCthulhu,
    Choices,
    ConExClusivePrints,
    Ghostbusters(GhostbustersBand),
    BackToTheFutureII,
    Labbit,
    Bombshells,
    GivenchyRewindCollective,
    Superman,
    CerealKillers,
    DcCoverGirls,
    BatmanBlackAndWhite,
    DragonGirl,
    Cryptkins,
    UltramanAnime,
    BackToTheFuture,
    Mermicornos,
    ToyTokyoGhostbusters,
    HarleyQuinnRedWhiteAndBlack,
    Unicornos,
    PowerpuffGirls,
    AdventureTime,
}

impl Display for Brand {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Brand::SpiderMan => write!(f, "Spider Man"),
            Brand::ToyTokyoAndJermaineRogers => write!(f, "Toy Tokyo X Jermain Rogers"),
            Brand::CallOfCthulhu => write!(f, "Call of Cthulhu"),
            Brand::Choices => write!(f, "Choices"),
            Brand::ConExClusivePrints => write!(f, "Con ExClusive Prints"),
            Brand::Ghostbusters(val) => write!(f, "Ghostbusters {}", val),
            Brand::BackToTheFutureII => write!(f, "Back to The Futuere II"),
            Brand::Labbit => write!(f, "Labbit"),
            Brand::Bombshells => write!(f, "Bombshells"),
            Brand::GivenchyRewindCollective => write!(f, "GIVENCHY X REWIND COLLECTIVE"),
            Brand::Superman => write!(f, "Superman"),
            Brand::CerealKillers => write!(f, "Cereal Killers"),
            Brand::DcCoverGirls => write!(f, "Dc Cover Girls"),
            Brand::BatmanBlackAndWhite => write!(f, "Batman Blach & White"),
            Brand::DragonGirl => write!(f, "DragonGirl"),
            Brand::Cryptkins => write!(f, "Cryptkins"),
            Brand::UltramanAnime => write!(f, "UltramanAnime"),
            Brand::BackToTheFuture => write!(f, "Back To The Future"),
            Brand::Mermicornos => write!(f, "Mermicornos"),
            Brand::ToyTokyoGhostbusters => write!(f, "ToyTokyo Ghostbusters"),
            Brand::HarleyQuinnRedWhiteAndBlack => write!(f, "Harley Quinn Red White & Black"),
            Brand::Unicornos => write!(f, "Unicornos"),
            Brand::PowerpuffGirls => write!(f, "PowerpuffGirls"),
            Brand::AdventureTime => write!(f, "Adventure Time"),
        }
    }
}

#[derive(Debug)]
pub enum GhostbustersBand {
    I,
}

impl Display for GhostbustersBand {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "I")
    }
}

#[derive(Debug)]
pub enum Series {
    ModernMarvel(ModerMarvel),
    Lucky,
    CthuluIdols,
    Choices(Choices),
    Mondo(Mondo),
    Ecto1,
    Hoverboards,
    Labbit(Labbit),
    Bombshell(Bombshell),
    GivenchyRewindCollective,
    Superman(Superman),
    CerealKillers(CerealKillers),
    DcCoverGirls(DcCoverGirls),
    BatmanBlackAndWhite(BatmanBlackAndWhite),
    DragonGirl,
    Cryptkins(Cryptkins),
    TheRiseOfUltraman(TheRiseOfUltraman),
    DeLoreanTimeMachine,
    Mermicornos(Mermicornos),
    Ultraman(Ultraman),
    ToyTokyoMooglie,
    HarleyQuinneRWB(HarleyQuinneRWB),
    GhostbustersI(GhostbustersISeries),
    Unicornos(Unicornos),
    PowerpuffGirls(PowerpuffGirls),
    AdventureTime(AdventureTime),
    BigBarda,
    Starfire,
}

impl Display for Series {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Series::ModernMarvel(val) => {
                write!(f, "Modern Marvel {}", val)
            }
            Series::Lucky => {
                write!(f, "Lucky")
            }
            Series::CthuluIdols => {
                write!(f, "Cthulu Idols")
            }
            Series::Choices(val) => {
                write!(f, "CHOICES {}", val)
            }
            Series::Mondo(val) => {
                write!(f, "Mondo {}", val)
            }
            Series::Ecto1 => {
                write!(f, "Ecto 1")
            }
            Series::Hoverboards => {
                write!(f, "Hoverboards")
            }
            Series::Labbit(val) => {
                write!(f, "Labbit {}", val)
            }
            Series::Bombshell(val) => {
                write!(f, "Bombshell {}", val)
            }
            Series::GivenchyRewindCollective => {
                write!(f, "GIVENCHY Rewind Collective")
            }
            Series::Superman(val) => {
                write!(f, "Superman {}", val)
            }
            Series::CerealKillers(val) => {
                write!(f, "Cerea Killers {}", val)
            }
            Series::DcCoverGirls(val) => {
                write!(f, "Dc Cover Girls {}", val)
            }
            Series::BatmanBlackAndWhite(val) => {
                write!(f, "Batman Black White {}", val)
            }
            Series::DragonGirl => {
                write!(f, "Dragon Girl")
            }
            Series::Cryptkins(val) => {
                write!(f, "Cryptkins {}", val)
            }
            Series::TheRiseOfUltraman(val) => {
                write!(f, "The Rise Of Ultraman {}", val)
            }
            Series::DeLoreanTimeMachine => {
                write!(f, "DeLoarean Time Machine")
            }
            Series::Mermicornos(val) => {
                write!(f, "Mermicornos {}", val)
            }
            Series::Ultraman(val) => {
                write!(f, "Ultraman {}", val)
            }
            Series::ToyTokyoMooglie => {
                write!(f, "Toy Tokyo Mooglie")
            }
            Series::HarleyQuinneRWB(val) => {
                write!(f, "Harley Quinne: RWB {}", val)
            }
            Series::GhostbustersI(val) => {
                write!(f, "Ghostbuster I {}", val)
            }
            Series::Unicornos(val) => {
                write!(f, "Unicornos {}", val)
            }
            Series::PowerpuffGirls(val) => {
                write!(f, "Powerpuff Girls {}", val)
            }
            Series::AdventureTime(val) => {
                write!(f, "Adventure Time {}", val)
            }
            Series::BigBarda => {
                write!(f, "BigBarda")
            }
            Series::Starfire => {
                write!(f, "Starfire")
            }
        }
    }
}

impl SeriesContent for Series {
    type Content = String;

    fn content(&self) -> Self::Content {
        match &self {
            Series::ModernMarvel(val) => match val {
                ModerMarvel::S1SpiderMan => {
                    "Introducing Marvel's first NFT, your friendly neighborhood Spider-Man! This \
                        is beautiful digital status is the debut collectible in the Modern Marvel \
                        series. Ready to swing into action to protect New York City, Marvel's \
                        premier Super Hero is presented in all his red-and-blue glory with this \
                        premium digital status! Do not miss out on the opportunity to start your \
                        Marvel collection with Aunt May's favorite nephew, the Amazing Spider-Man.\
                        \
                        The series is available globally.
                        "
                    .to_string()
                }
            },
            Series::Lucky => {
                unimplemented!()
            }
            Series::CthuluIdols => {
                unimplemented!()
            }
            Series::Choices(_) => {
                unimplemented!()
            }
            Series::Mondo(_) => {
                unimplemented!()
            }
            Series::Ecto1 => {
                unimplemented!()
            }
            Series::Hoverboards => {
                unimplemented!()
            }
            Series::Labbit(_) => {
                unimplemented!()
            }
            Series::Bombshell(_) => {
                unimplemented!()
            }
            Series::GivenchyRewindCollective => {
                unimplemented!()
            }
            Series::Superman(_) => {
                unimplemented!()
            }
            Series::CerealKillers(_) => {
                unimplemented!()
            }
            Series::DcCoverGirls(_) => {
                unimplemented!()
            }
            Series::BatmanBlackAndWhite(_) => {
                unimplemented!()
            }
            Series::DragonGirl => {
                unimplemented!()
            }
            Series::Cryptkins(_) => {
                unimplemented!()
            }
            Series::TheRiseOfUltraman(_) => {
                unimplemented!()
            }
            Series::DeLoreanTimeMachine => {
                unimplemented!()
            }
            Series::Mermicornos(_) => {
                unimplemented!()
            }
            Series::Ultraman(_) => {
                unimplemented!()
            }
            Series::ToyTokyoMooglie => {
                unimplemented!()
            }
            Series::HarleyQuinneRWB(_) => {
                unimplemented!()
            }
            Series::GhostbustersI(_) => {
                unimplemented!()
            }
            Series::Unicornos(_) => {
                unimplemented!()
            }
            Series::PowerpuffGirls(_) => {
                unimplemented!()
            }
            Series::AdventureTime(_) => {
                unimplemented!()
            }
            Series::BigBarda => {
                unimplemented!()
            }
            Series::Starfire => {
                unimplemented!()
            }
        }
    }
}

#[derive(Debug)]
pub enum ModerMarvel {
    S1SpiderMan,
}

impl Display for ModerMarvel {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            ModerMarvel::S1SpiderMan => write!(f, "S1 Spider Man"),
        }
    }
}

#[derive(Debug)]
pub enum Choices {
    S1,
}

impl Display for Choices {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Choices::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum Mondo {
    S1,
}

impl Display for Mondo {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Mondo::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum Labbit {
    S1,
}

impl Display for Labbit {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Labbit::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum Bombshell {
    S1,
}

impl Display for Bombshell {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Bombshell::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum Superman {
    S1,
}

impl Display for Superman {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Superman::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum CerealKillers {
    S1,
}
impl Display for CerealKillers {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            CerealKillers::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum DcCoverGirls {
    S1,
}
impl Display for DcCoverGirls {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            DcCoverGirls::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum BatmanBlackAndWhite {
    S1,
    S2,
    S3,
    S4,
    S5,
}
impl Display for BatmanBlackAndWhite {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            BatmanBlackAndWhite::S1 => write!(f, "S1"),
            BatmanBlackAndWhite::S2 => write!(f, "S2"),
            BatmanBlackAndWhite::S3 => write!(f, "S3"),
            BatmanBlackAndWhite::S4 => write!(f, "S4"),
            BatmanBlackAndWhite::S5 => write!(f, "S5"),
        }
    }
}

#[derive(Debug)]
pub enum Cryptkins {
    S1,
}

impl Display for Cryptkins {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Cryptkins::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum TheRiseOfUltraman {
    S1,
}

impl Display for TheRiseOfUltraman {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            TheRiseOfUltraman::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum Mermicornos {
    S1,
}

impl Display for Mermicornos {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Mermicornos::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum Ultraman {
    S1,
}

impl Display for Ultraman {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Ultraman::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum HarleyQuinneRWB {
    S1,
}

impl Display for HarleyQuinneRWB {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            HarleyQuinneRWB::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum GhostbustersISeries {
    S1,
}
impl Display for GhostbustersISeries {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            GhostbustersISeries::S1 => write!(f, "S1"),
        }
    }
}

#[derive(Debug)]
pub enum Unicornos {
    S1,
}

impl Display for Unicornos {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            Unicornos::S1 => write!(f, "S1"),
        }
    }
}
#[derive(Debug)]
pub enum PowerpuffGirls {
    S1,
    S2,
}

impl Display for PowerpuffGirls {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            PowerpuffGirls::S1 => write!(f, "S1"),
            PowerpuffGirls::S2 => write!(f, "S2"),
        }
    }
}

#[derive(Debug)]
pub enum AdventureTime {
    S1,
    S2,
}

impl Display for AdventureTime {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            AdventureTime::S1 => write!(f, "S1"),
            AdventureTime::S2 => write!(f, "S2"),
        }
    }
}
