use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
use crate::series::Object;
use uuid::Uuid;

#[derive(Debug)]
pub enum Choices {
    TheProtector(TheProtector),
    TheAssassin(TheAssassin),
    TheNightwatch(TheNightwatch),
    TheHunter(TheHunter),
}

impl Object for Choices {
    type Name = String;
    type DropDate = String;
    type ListPrice = f32;
    type Editions = u32;
    type EditionType = EditionType;
    type Season = Season;
    type Rarity = Rarity;
    type License = License;
    type Brand = Brand;
    type Series = Series;
    type Uuid = Uuid;

    fn get_name(&self) -> Self::Name {
        match &self {
            Choices::TheHunter(val) => val.name.clone(),
            Choices::TheNightwatch(val) => val.name.clone(),
            Choices::TheAssassin(val) => val.name.clone(),
            Choices::TheProtector(val) => val.name.clone(),
        }
    }

    fn get_drop_date(&self) -> Self::DropDate {
        match &self {
            Choices::TheHunter(val) => val.drop_date.clone(),
            Choices::TheNightwatch(val) => val.drop_date.clone(),
            Choices::TheAssassin(val) => val.drop_date.clone(),
            Choices::TheProtector(val) => val.drop_date.clone(),
        }
    }

    fn get_list_price(&self) -> Self::ListPrice {
        match &self {
            Choices::TheHunter(val) => val.list_price.clone(),
            Choices::TheNightwatch(val) => val.list_price.clone(),
            Choices::TheAssassin(val) => val.list_price.clone(),
            Choices::TheProtector(val) => val.list_price.clone(),
        }
    }

    fn get_editions(&self) -> Self::Editions {
        match &self {
            Choices::TheHunter(val) => val.editions.clone(),
            Choices::TheNightwatch(val) => val.editions.clone(),
            Choices::TheAssassin(val) => val.editions.clone(),
            Choices::TheProtector(val) => val.editions.clone(),
        }
    }

    fn get_edition_type(&self) -> Self::EditionType {
        EditionType::ConExclusive
    }

    fn get_season(&self) -> Self::Season {
        Season::Season2
    }

    fn get_rarity(&self) -> Self::Rarity {
        match &self {
            Choices::TheHunter(val) => Rarity::UltraRare,
            Choices::TheNightwatch(val) => Rarity::Rare,
            Choices::TheAssassin(val) => Rarity::Uncommon,
            Choices::TheProtector(val) => Rarity::Common,
        }
    }

    fn get_license(&self) -> Self::License {
        Self::License::JermaineRogers
    }

    fn get_brand(&self) -> Self::Brand {
        Self::Brand::Choices
    }

    fn get_series(&self) -> Self::Series {
        Self::Series::Choices(crate::series::common_type::Choices::S1)
    }

    fn get_uuid(&self) -> Self::Uuid {
        match &self {
            Choices::TheHunter(val) => val.id.clone(),
            Choices::TheNightwatch(val) => val.id.clone(),
            Choices::TheAssassin(val) => val.id.clone(),
            Choices::TheProtector(val) => val.id.clone(),
        }
    }
}

#[derive(Debug)]
pub struct TheProtector {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl TheProtector {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct TheAssassin {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl TheAssassin {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct TheNightwatch {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl TheNightwatch {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct TheHunter {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl TheHunter {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
