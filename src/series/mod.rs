pub mod cartoon_network;
pub mod chaosium;
pub mod cryptkins;
pub mod dc_direct;
pub mod frank_kozik;
pub mod ghostbusters;
pub mod givenchy;
pub mod jermaine_rogers;

/// license accounting by license
pub mod marvel;
pub mod mondo;
pub mod ron_english;
pub mod tokidoki;
pub mod toy_tokyo;
pub mod tsuburaya;
pub mod universal;

/// Define common type
pub mod common_type;

use std::fmt::Display;
use std::fmt::{Debug, Formatter};

/// Define object trait for VeVe series object common function
pub trait Object: Debug {
    type Name: Debug + Display;
    type DropDate: Debug + Display;
    type ListPrice: Debug + Display;
    type Editions: Debug + Display;
    type EditionType: Debug + Display;
    type Season: Debug + Display;
    type Rarity: Debug + Display;
    type License: Debug + Display;
    type Brand: Debug + Display;
    type Series: Debug + Display;
    type Uuid: Debug;

    /// Get object name
    fn get_name(&self) -> Self::Name;

    /// Get object drop date
    fn get_drop_date(&self) -> Self::DropDate;

    /// Get object list price
    fn get_list_price(&self) -> Self::ListPrice;

    /// Get object editions
    fn get_editions(&self) -> Self::Editions;

    /// Get object edition type
    fn get_edition_type(&self) -> Self::EditionType;

    /// Get object season
    fn get_season(&self) -> Self::Season;

    /// Get object rarity
    fn get_rarity(&self) -> Self::Rarity;

    /// Get object license
    fn get_license(&self) -> Self::License;

    /// Get object brand
    fn get_brand(&self) -> Self::Brand;

    /// get object series
    fn get_series(&self) -> Self::Series;

    /// get object uuid
    fn get_uuid(&self) -> Self::Uuid;
}

// impl Display for dyn Object {
//     fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
//         writeln!(f, "Name: {}", self.get_name());
//         writeln!(f, "DropDate: {}", self.get_drop_date());
//         writeln!(f, "ListPrice: {}", self.get_list_price());
//         writeln!(f, "Editions: {}", self.get_editions());
//         writeln!(f, "EditionType: {}", self.get_edition_type());
//         writeln!(f, "Season: {}", self.get_season());
//         writeln!(f, "Rarity: {}", self.get_rarity());
//         writeln!(f, "License: {}", self.get_license());
//         writeln!(f, "Brand: {}", self.get_brand());
//         writeln!(f, "Series: {}", self.get_series())
//     }
// }

pub trait MarketPrice {
    type Price;

    fn get_market_price(&self) -> Self::Price;
}

pub trait SeriesContent {
    type Content;

    fn content(&self) -> Self::Content;
}

pub trait PicSource {
    type Source;

    fn source(&self) -> Self::Source;
}
