use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
use crate::series::Object;
use uuid::Uuid;

#[derive(Debug)]
pub struct Lucky {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}

impl Lucky {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

impl Object for Lucky {
    type Name = String;
    type DropDate = String;
    type ListPrice = f32;
    type Editions = u32;
    type EditionType = EditionType;
    type Season = Season;
    type Rarity = Rarity;
    type License = License;
    type Brand = Brand;
    type Series = Series;
    type Uuid = Uuid;

    fn get_name(&self) -> Self::Name {
        self.name.clone()
    }

    fn get_drop_date(&self) -> Self::DropDate {
        self.drop_date.clone()
    }

    fn get_list_price(&self) -> Self::ListPrice {
        self.list_price.clone()
    }

    fn get_editions(&self) -> Self::Editions {
        self.editions.clone()
    }

    fn get_edition_type(&self) -> Self::EditionType {
        EditionType::ConExclusive
    }

    fn get_season(&self) -> Self::Season {
        Season::Season2
    }

    fn get_rarity(&self) -> Self::Rarity {
        Rarity::SecretRare
    }

    fn get_license(&self) -> Self::License {
        License::ToyTokyo
    }

    fn get_brand(&self) -> Self::Brand {
        Brand::ToyTokyoAndJermaineRogers
    }

    fn get_series(&self) -> Self::Series {
        Series::Lucky
    }

    fn get_uuid(&self) -> Self::Uuid {
        self.id.clone()
    }
}
