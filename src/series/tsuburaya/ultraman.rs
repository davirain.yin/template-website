mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum Ultraman {
        UltramanCommon(UltramanCommon),
        UltramanSeevenUncommon(UltramanSeevenUncommon),
        UltramanAceRare(UltramanAceRare),
        EdoUltraRare(EdoUltraRare),
    }

    impl Object for Ultraman {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                Ultraman::UltramanCommon(val) => val.name.clone(),
                Ultraman::UltramanSeevenUncommon(val) => val.name.clone(),
                Ultraman::UltramanAceRare(val) => val.name.clone(),
                Ultraman::EdoUltraRare(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                Ultraman::UltramanCommon(val) => val.drop_date.clone(),
                Ultraman::UltramanSeevenUncommon(val) => val.drop_date.clone(),
                Ultraman::UltramanAceRare(val) => val.drop_date.clone(),
                Ultraman::EdoUltraRare(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                Ultraman::UltramanCommon(val) => val.list_price.clone(),
                Ultraman::UltramanSeevenUncommon(val) => val.list_price.clone(),
                Ultraman::UltramanAceRare(val) => val.list_price.clone(),
                Ultraman::EdoUltraRare(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                Ultraman::UltramanCommon(val) => val.editions.clone(),
                Ultraman::UltramanSeevenUncommon(val) => val.editions.clone(),
                Ultraman::UltramanAceRare(val) => val.editions.clone(),
                Ultraman::EdoUltraRare(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                Ultraman::UltramanCommon(val) => Rarity::Common,
                Ultraman::UltramanSeevenUncommon(val) => Rarity::Uncommon,
                Ultraman::UltramanAceRare(val) => Rarity::Rare,
                Ultraman::EdoUltraRare(val) => Rarity::UltraRare,
            }
        }

        fn get_license(&self) -> Self::License {
            License::Tsuburaya
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::UltramanAnime
        }

        fn get_series(&self) -> Self::Series {
            Series::Ultraman(crate::series::common_type::Ultraman::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                Ultraman::UltramanCommon(val) => val.id.clone(),
                Ultraman::UltramanSeevenUncommon(val) => val.id.clone(),
                Ultraman::UltramanAceRare(val) => val.id.clone(),
                Ultraman::EdoUltraRare(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct UltramanCommon {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl UltramanCommon {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct UltramanSeevenUncommon {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl UltramanSeevenUncommon {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct UltramanAceRare {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl UltramanAceRare {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct EdoUltraRare {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl EdoUltraRare {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
