mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum TheRiseOfUltraman {
        AlexRoss1(AlexRoss1),
        JohnTylerChristopher1(JohnTylerChristopher1),
        SkottieYoung1(SkottieYoung1),
        EdMcGuinness1(EdMcGuinness1),
        AdiGranov1(AdiGranov1),
        YujiKaida1(YujiKaida1),
    }

    impl Object for TheRiseOfUltraman {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                TheRiseOfUltraman::AlexRoss1(val) => val.name.clone(),
                TheRiseOfUltraman::JohnTylerChristopher1(val) => val.name.clone(),
                TheRiseOfUltraman::SkottieYoung1(val) => val.name.clone(),
                TheRiseOfUltraman::EdMcGuinness1(val) => val.name.clone(),
                TheRiseOfUltraman::AdiGranov1(val) => val.name.clone(),
                TheRiseOfUltraman::YujiKaida1(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                TheRiseOfUltraman::AlexRoss1(val) => val.drop_date.clone(),
                TheRiseOfUltraman::JohnTylerChristopher1(val) => val.drop_date.clone(),
                TheRiseOfUltraman::SkottieYoung1(val) => val.drop_date.clone(),
                TheRiseOfUltraman::EdMcGuinness1(val) => val.drop_date.clone(),
                TheRiseOfUltraman::AdiGranov1(val) => val.drop_date.clone(),
                TheRiseOfUltraman::YujiKaida1(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                TheRiseOfUltraman::AlexRoss1(val) => val.list_price.clone(),
                TheRiseOfUltraman::JohnTylerChristopher1(val) => val.list_price.clone(),
                TheRiseOfUltraman::SkottieYoung1(val) => val.list_price.clone(),
                TheRiseOfUltraman::EdMcGuinness1(val) => val.list_price.clone(),
                TheRiseOfUltraman::AdiGranov1(val) => val.list_price.clone(),
                TheRiseOfUltraman::YujiKaida1(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                TheRiseOfUltraman::AlexRoss1(val) => val.editions.clone(),
                TheRiseOfUltraman::JohnTylerChristopher1(val) => val.editions.clone(),
                TheRiseOfUltraman::SkottieYoung1(val) => val.editions.clone(),
                TheRiseOfUltraman::EdMcGuinness1(val) => val.editions.clone(),
                TheRiseOfUltraman::AdiGranov1(val) => val.editions.clone(),
                TheRiseOfUltraman::YujiKaida1(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstEdition
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                TheRiseOfUltraman::AlexRoss1(val) => Rarity::Common,
                TheRiseOfUltraman::JohnTylerChristopher1(val) => Rarity::SecretRare,
                TheRiseOfUltraman::SkottieYoung1(val) => Rarity::Uncommon,
                TheRiseOfUltraman::EdMcGuinness1(val) => Rarity::Common,
                TheRiseOfUltraman::AdiGranov1(val) => Rarity::Rare,
                TheRiseOfUltraman::YujiKaida1(val) => Rarity::Uncommon,
            }
        }

        fn get_license(&self) -> Self::License {
            License::Tsuburaya
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::UltramanAnime
        }

        fn get_series(&self) -> Self::Series {
            Series::TheRiseOfUltraman(crate::series::common_type::TheRiseOfUltraman::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                TheRiseOfUltraman::AlexRoss1(val) => val.id.clone(),
                TheRiseOfUltraman::JohnTylerChristopher1(val) => val.id.clone(),
                TheRiseOfUltraman::SkottieYoung1(val) => val.id.clone(),
                TheRiseOfUltraman::EdMcGuinness1(val) => val.id.clone(),
                TheRiseOfUltraman::AdiGranov1(val) => val.id.clone(),
                TheRiseOfUltraman::YujiKaida1(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct AlexRoss1 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl AlexRoss1 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct JohnTylerChristopher1 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl JohnTylerChristopher1 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct SkottieYoung1 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl SkottieYoung1 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct EdMcGuinness1 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl EdMcGuinness1 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct AdiGranov1 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl AdiGranov1 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct YujiKaida1 {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl YujiKaida1 {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
