mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;
    use uuid::Uuid;

    #[derive(Debug)]
    pub enum Mermicorno {
        Gelatina(Gelatina),
        Marina(Marina),
        Tropicana(Tropicana),
        Perla(Perla),
        Marea(Marea),
        Oceania(Oceania),
        Sirena(Sirena),
        Cora(Cora),
        Corsica(Corsica),
    }

    impl Object for Mermicorno {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                Mermicorno::Gelatina(val) => val.name.clone(),
                Mermicorno::Marina(val) => val.name.clone(),
                Mermicorno::Tropicana(val) => val.name.clone(),
                Mermicorno::Perla(val) => val.name.clone(),
                Mermicorno::Marea(val) => val.name.clone(),
                Mermicorno::Oceania(val) => val.name.clone(),
                Mermicorno::Sirena(val) => val.name.clone(),
                Mermicorno::Cora(val) => val.name.clone(),
                Mermicorno::Corsica(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                Mermicorno::Gelatina(val) => val.drop_date.clone(),
                Mermicorno::Marina(val) => val.drop_date.clone(),
                Mermicorno::Tropicana(val) => val.drop_date.clone(),
                Mermicorno::Perla(val) => val.drop_date.clone(),
                Mermicorno::Marea(val) => val.drop_date.clone(),
                Mermicorno::Oceania(val) => val.drop_date.clone(),
                Mermicorno::Sirena(val) => val.drop_date.clone(),
                Mermicorno::Cora(val) => val.drop_date.clone(),
                Mermicorno::Corsica(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                Mermicorno::Gelatina(val) => val.list_price.clone(),
                Mermicorno::Marina(val) => val.list_price.clone(),
                Mermicorno::Tropicana(val) => val.list_price.clone(),
                Mermicorno::Perla(val) => val.list_price.clone(),
                Mermicorno::Marea(val) => val.list_price.clone(),
                Mermicorno::Oceania(val) => val.list_price.clone(),
                Mermicorno::Sirena(val) => val.list_price.clone(),
                Mermicorno::Cora(val) => val.list_price.clone(),
                Mermicorno::Corsica(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                Mermicorno::Gelatina(val) => val.editions.clone(),
                Mermicorno::Marina(val) => val.editions.clone(),
                Mermicorno::Tropicana(val) => val.editions.clone(),
                Mermicorno::Perla(val) => val.editions.clone(),
                Mermicorno::Marea(val) => val.editions.clone(),
                Mermicorno::Oceania(val) => val.editions.clone(),
                Mermicorno::Sirena(val) => val.editions.clone(),
                Mermicorno::Cora(val) => val.editions.clone(),
                Mermicorno::Corsica(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                Mermicorno::Gelatina(val) => Rarity::Common,
                Mermicorno::Marina(val) => Rarity::Uncommon,
                Mermicorno::Tropicana(val) => Rarity::Rare,
                Mermicorno::Perla(val) => Rarity::Uncommon,
                Mermicorno::Marea(val) => Rarity::Uncommon,
                Mermicorno::Oceania(val) => Rarity::Common,
                Mermicorno::Sirena(val) => Rarity::Common,
                Mermicorno::Cora(val) => Rarity::Common,
                Mermicorno::Corsica(val) => Rarity::UltraRare,
            }
        }

        fn get_license(&self) -> Self::License {
            License::Tokidoki
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::Mermicornos
        }

        fn get_series(&self) -> Self::Series {
            Series::Mermicornos(crate::series::common_type::Mermicornos::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                Mermicorno::Gelatina(val) => val.id.clone(),
                Mermicorno::Marina(val) => val.id.clone(),
                Mermicorno::Tropicana(val) => val.id.clone(),
                Mermicorno::Perla(val) => val.id.clone(),
                Mermicorno::Marea(val) => val.id.clone(),
                Mermicorno::Oceania(val) => val.id.clone(),
                Mermicorno::Sirena(val) => val.id.clone(),
                Mermicorno::Cora(val) => val.id.clone(),
                Mermicorno::Corsica(val) => val.id.clone(),
            }
        }
    }
    #[derive(Debug)]
    pub struct Gelatina {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Gelatina {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Marina {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Marina {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Tropicana {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Tropicana {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
    #[derive(Debug)]
    pub struct Perla {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Perla {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Marea {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Marea {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Oceania {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Oceania {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
    #[derive(Debug)]
    pub struct Sirena {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Sirena {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Cora {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Cora {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Corsica {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Corsica {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
