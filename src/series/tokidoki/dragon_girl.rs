use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
use crate::series::Object;
use uuid::Uuid;

#[derive(Debug)]
pub enum DragonGirl {
    GoldPink(GoldPink),
    SilverGold(SilverGold),
    Silver(Silver),
    SilverBlack(SilverBlack),
}

impl Object for DragonGirl {
    type Name = String;
    type DropDate = String;
    type ListPrice = f32;
    type Editions = u32;
    type EditionType = EditionType;
    type Season = Season;
    type Rarity = Rarity;
    type License = License;
    type Brand = Brand;
    type Series = Series;
    type Uuid = Uuid;

    fn get_name(&self) -> Self::Name {
        match &self {
            DragonGirl::GoldPink(val) => val.name.clone(),
            DragonGirl::SilverGold(val) => val.name.clone(),
            DragonGirl::Silver(val) => val.name.clone(),
            DragonGirl::SilverBlack(val) => val.name.clone(),
        }
    }

    fn get_drop_date(&self) -> Self::DropDate {
        match &self {
            DragonGirl::GoldPink(val) => val.drop_date.clone(),
            DragonGirl::SilverGold(val) => val.drop_date.clone(),
            DragonGirl::Silver(val) => val.drop_date.clone(),
            DragonGirl::SilverBlack(val) => val.drop_date.clone(),
        }
    }

    fn get_list_price(&self) -> Self::ListPrice {
        match &self {
            DragonGirl::GoldPink(val) => val.list_price.clone(),
            DragonGirl::SilverGold(val) => val.list_price.clone(),
            DragonGirl::Silver(val) => val.list_price.clone(),
            DragonGirl::SilverBlack(val) => val.list_price.clone(),
        }
    }

    fn get_editions(&self) -> Self::Editions {
        match &self {
            DragonGirl::GoldPink(val) => val.editions.clone(),
            DragonGirl::SilverGold(val) => val.editions.clone(),
            DragonGirl::Silver(val) => val.editions.clone(),
            DragonGirl::SilverBlack(val) => val.editions.clone(),
        }
    }

    fn get_edition_type(&self) -> Self::EditionType {
        EditionType::FirstAppearance
    }

    fn get_season(&self) -> Self::Season {
        Season::Season1
    }

    fn get_rarity(&self) -> Self::Rarity {
        match &self {
            DragonGirl::GoldPink(val) => Rarity::Common,
            DragonGirl::SilverGold(val) => Rarity::UltraRare,
            DragonGirl::Silver(val) => Rarity::Rare,
            DragonGirl::SilverBlack(val) => Rarity::Uncommon,
        }
    }

    fn get_license(&self) -> Self::License {
        License::Tokidoki
    }

    fn get_brand(&self) -> Self::Brand {
        Brand::DragonGirl
    }

    fn get_series(&self) -> Self::Series {
        Series::DragonGirl
    }

    fn get_uuid(&self) -> Self::Uuid {
        match &self {
            DragonGirl::GoldPink(val) => val.id.clone(),
            DragonGirl::SilverGold(val) => val.id.clone(),
            DragonGirl::Silver(val) => val.id.clone(),
            DragonGirl::SilverBlack(val) => val.id.clone(),
        }
    }
}

#[derive(Debug)]
pub struct GoldPink {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl GoldPink {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct SilverGold {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl SilverGold {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct Silver {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl Silver {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub struct SilverBlack {
    id: Uuid,
    name: String,
    drop_date: String,
    list_price: f32,
    editions: u32,
}
impl SilverBlack {
    pub fn new(id: Uuid) -> Self {
        todo!()
    }
}
