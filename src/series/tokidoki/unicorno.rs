mod s1 {
    use crate::series::common_type::{Brand, EditionType, License, Rarity, Season, Series};
    use crate::series::Object;

    use uuid::Uuid;

    #[derive(Debug)]
    pub enum Unicorno {
        Mokka(Mokka),
        PrimaDonna(PrimaDonna),
        Ritmo(Ritmo),
        Pogo(Pogo),
        Dolce(Dolce),
        Stellina(Stellina),
        Bellina(Bellina),
        Metallo(Metallo),
        Fumo(Fumo),
        Peperino(Peperino),
    }

    impl Object for Unicorno {
        type Name = String;
        type DropDate = String;
        type ListPrice = f32;
        type Editions = u32;
        type EditionType = EditionType;
        type Season = Season;
        type Rarity = Rarity;
        type License = License;
        type Brand = Brand;
        type Series = Series;
        type Uuid = Uuid;

        fn get_name(&self) -> Self::Name {
            match &self {
                Unicorno::Mokka(val) => val.name.clone(),
                Unicorno::PrimaDonna(val) => val.name.clone(),
                Unicorno::Ritmo(val) => val.name.clone(),
                Unicorno::Pogo(val) => val.name.clone(),
                Unicorno::Dolce(val) => val.name.clone(),
                Unicorno::Stellina(val) => val.name.clone(),
                Unicorno::Bellina(val) => val.name.clone(),
                Unicorno::Metallo(val) => val.name.clone(),
                Unicorno::Fumo(val) => val.name.clone(),
                Unicorno::Peperino(val) => val.name.clone(),
            }
        }

        fn get_drop_date(&self) -> Self::DropDate {
            match &self {
                Unicorno::Mokka(val) => val.drop_date.clone(),
                Unicorno::PrimaDonna(val) => val.drop_date.clone(),
                Unicorno::Ritmo(val) => val.drop_date.clone(),
                Unicorno::Pogo(val) => val.drop_date.clone(),
                Unicorno::Dolce(val) => val.drop_date.clone(),
                Unicorno::Stellina(val) => val.drop_date.clone(),
                Unicorno::Bellina(val) => val.drop_date.clone(),
                Unicorno::Metallo(val) => val.drop_date.clone(),
                Unicorno::Fumo(val) => val.drop_date.clone(),
                Unicorno::Peperino(val) => val.drop_date.clone(),
            }
        }

        fn get_list_price(&self) -> Self::ListPrice {
            match &self {
                Unicorno::Mokka(val) => val.list_price.clone(),
                Unicorno::PrimaDonna(val) => val.list_price.clone(),
                Unicorno::Ritmo(val) => val.list_price.clone(),
                Unicorno::Pogo(val) => val.list_price.clone(),
                Unicorno::Dolce(val) => val.list_price.clone(),
                Unicorno::Stellina(val) => val.list_price.clone(),
                Unicorno::Bellina(val) => val.list_price.clone(),
                Unicorno::Metallo(val) => val.list_price.clone(),
                Unicorno::Fumo(val) => val.list_price.clone(),
                Unicorno::Peperino(val) => val.list_price.clone(),
            }
        }

        fn get_editions(&self) -> Self::Editions {
            match &self {
                Unicorno::Mokka(val) => val.editions.clone(),
                Unicorno::PrimaDonna(val) => val.editions.clone(),
                Unicorno::Ritmo(val) => val.editions.clone(),
                Unicorno::Pogo(val) => val.editions.clone(),
                Unicorno::Dolce(val) => val.editions.clone(),
                Unicorno::Stellina(val) => val.editions.clone(),
                Unicorno::Bellina(val) => val.editions.clone(),
                Unicorno::Metallo(val) => val.editions.clone(),
                Unicorno::Fumo(val) => val.editions.clone(),
                Unicorno::Peperino(val) => val.editions.clone(),
            }
        }

        fn get_edition_type(&self) -> Self::EditionType {
            EditionType::FirstAppearance
        }

        fn get_season(&self) -> Self::Season {
            Season::Season1
        }

        fn get_rarity(&self) -> Self::Rarity {
            match &self {
                Unicorno::Mokka(val) => Rarity::Common,
                Unicorno::PrimaDonna(val) => Rarity::Common,
                Unicorno::Ritmo(val) => Rarity::UltraRare,
                Unicorno::Pogo(val) => Rarity::Uncommon,
                Unicorno::Dolce(val) => Rarity::Common,
                Unicorno::Stellina(val) => Rarity::Common,
                Unicorno::Bellina(val) => Rarity::Rare,
                Unicorno::Metallo(val) => Rarity::Rare,
                Unicorno::Fumo(val) => Rarity::Uncommon,
                Unicorno::Peperino(val) => Rarity::Uncommon,
            }
        }

        fn get_license(&self) -> Self::License {
            License::Tokidoki
        }

        fn get_brand(&self) -> Self::Brand {
            Brand::Unicornos
        }

        fn get_series(&self) -> Self::Series {
            Series::Unicornos(crate::series::common_type::Unicornos::S1)
        }

        fn get_uuid(&self) -> Self::Uuid {
            match &self {
                Unicorno::Mokka(val) => val.id.clone(),
                Unicorno::PrimaDonna(val) => val.id.clone(),
                Unicorno::Ritmo(val) => val.id.clone(),
                Unicorno::Pogo(val) => val.id.clone(),
                Unicorno::Dolce(val) => val.id.clone(),
                Unicorno::Stellina(val) => val.id.clone(),
                Unicorno::Bellina(val) => val.id.clone(),
                Unicorno::Metallo(val) => val.id.clone(),
                Unicorno::Fumo(val) => val.id.clone(),
                Unicorno::Peperino(val) => val.id.clone(),
            }
        }
    }

    #[derive(Debug)]
    pub struct Mokka {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Mokka {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
    #[derive(Debug)]
    pub struct PrimaDonna {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl PrimaDonna {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
    #[derive(Debug)]
    pub struct Ritmo {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Ritmo {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Pogo {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Pogo {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
    #[derive(Debug)]
    pub struct Dolce {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }

    impl Dolce {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
    #[derive(Debug)]
    pub struct Stellina {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Stellina {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Bellina {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Bellina {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Metallo {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Metallo {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Fumo {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Fumo {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }

    #[derive(Debug)]
    pub struct Peperino {
        id: Uuid,
        name: String,
        drop_date: String,
        list_price: f32,
        editions: u32,
    }
    impl Peperino {
        pub fn new(id: Uuid) -> Self {
            todo!()
        }
    }
}
